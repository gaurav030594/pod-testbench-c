
#include "cndklib.h"
#include "debugflags.h"
#include "testcases.h"

extern int testcase;		//extern variable for testcase
/////////////////////////////////POD Testing functions////////////////////////////////////////////////////////////////////

//char *playSonic30Byte(char data[],char bufferRet[],int size1);
void checkIndexMatch(char SrlIndexFrmPOD[],int IndexRecieved[])
{
	int i=0;
	int comp=0;
	char retBuff[SIZEQ+1]={0};
	FILE *fp;//,*fp1;
//	static int notDetect=0;

	memset(freqsToSend,'\0',sizeof(freqsToSend));
	/*fp1=fopen("notDetect.txt","w");
	if(fp1==NULL){printf("File not opened\n");}
	notDetect++;
	fprintf(fp1," %d",notDetect);
	fclose(fp1);*/
#ifdef DEBUG
	printf("output: %s\n",output);
#endif
	fp=fopen(output,"a+");
	if(fp==NULL){printf("File not opened in checkIndexMatch\n");}

	playSonic30Byte(randData,retBuff,SIZEQ);
	fprintf(fp,"Systm Data: %s\n",randData);
	//playSonic30Byte("D718470911",retBuff,size);

#ifdef DEV_DEBUG_L1
	printf("freqsToSend\n");
	fprintf(fp,"%s\n","freqsToSend");
	for(i=0;i<40;i++){printf("%d ",freqsToSend[i]);fprintf(fp," %d",freqsToSend[i]);}
	fprintf(fp,"%c",'\n');
	printf("\n");

	printf("SrlIndexFrmPOD\n");
	fprintf(fp,"%s\n","SrlIndexFrmPOD");
	for(i=0;i<40;i++){printf("%d ",SrlIndexFrmPOD[i]);fprintf(fp," %d",SrlIndexFrmPOD[i]);}
	fprintf(fp,"%c",'\n');
	printf("\n");
#endif
	for(i=0;i<40;i++){
		if(SrlIndexFrmPOD[i]!=freqsToSend[i]){
			comp=1;
		}
	}

	if(comp==0){printf("OFreq and Sfreq compared\n");
	}else{printf("OFreq and Sfreq not compared\n");fprintf(fp,"%s\n","OFreq and Sfreq not compared");}
	printf("IndexRecievedFrmMic\n");
	fprintf(fp,"%s\n","IndexRecievedFrmMic");
	for(i=0;i<40;i++){printf("%d ",IndexRecieved[i]);fprintf(fp," %d",IndexRecieved[i]);}
	fprintf(fp,"%c",'\n');
	printf("\n");

        printf("I:   V:   E:\n");
	fprintf(fp,"%s\n","I:   V:   E:");
	for(i=0;i<40;i++){
		if(IndexRecieved[i]!=SrlIndexFrmPOD[i]){
			fprintf(fp,"%d    ",i);
			printf("%d    ",i);
			fprintf(fp,"%d    ",IndexRecieved[i]);
			printf("%d    ",IndexRecieved[i]);
			fprintf(fp,"%d    \n\n",SrlIndexFrmPOD[i]);
			printf("%d    \n",SrlIndexFrmPOD[i]);

		}
	}


	//printf("%s\n",seperator);
	//fprintf(fp,"%s\n",seperator);
	fclose(fp);
}
/////////////////////////////////POD Testing functions  end///////////////////////////////////////////////////////////////


///////////////////////////////////////////////////UTILS STARTS////////////////////////////////////////////////////////////
unsigned char key1[]   = {0xA4,0x83,0x54,0x6F,0x38,0xC5,0xA6,0x6A,0x50,0x11,0x7C,0xF2,0xB0,0xDD,0x31,0xDC};
int digitCodeMappingInitiaized;
int charCodeMappingInitiaized;
char sCharsToCodes[256];
char sCodesToChars[256];
char sDigitToCodes[256];
char sCodesToDigit[256];

void initializeCharCodeMapping() {
    if (charCodeMappingInitiaized)
        return;

    char i = 0;

    int ii;
    for (ii = 0; ii < 256; ii++) {
        sCharsToCodes[ii] = -1;
        sCodesToChars[ii] = '\0';
    }

    sCharsToCodes['\0'] = i;
    sCodesToChars[i] = '\0';

    char c;
    for (c = 'a'; c <= 'z'; c++) {
        i++;
        sCharsToCodes[c] = i;
        sCodesToChars[i] = c;
    }

    for (c = 'A'; c <= 'Z'; c++) {
        i++;
        sCharsToCodes[c] = i;
        sCodesToChars[i] = c;
    }

    for (c = '0'; c <= '9'; c++) {
        i++;
        sCharsToCodes[c] = i;
        sCodesToChars[i] = c;
    }

    i++;
    sCharsToCodes[' '] = i;
    sCodesToChars[i] = ' ';

    charCodeMappingInitiaized = 1;
}

void initializeDigitCodeMapping() {
    if (digitCodeMappingInitiaized)
        return;

    int ii;
    for (ii = 0; ii < 256; ii++) {
        sDigitToCodes[ii] = -1;
        sCodesToDigit[ii] = '\0';
    }

    char i = 0;
    sDigitToCodes['\0'] = i;
    sCodesToDigit[i] = '\0';

    char c;
    for (c = '0'; c <= '9'; c++) {
        i++;
        sDigitToCodes[c] = i;
        sCodesToDigit[i] = c;
    }
    for (c = 'A'; c <= 'D'; c++) {
        i++;
        sDigitToCodes[c] = i;
        sCodesToDigit[i] = c;
    }

    i++;
    sDigitToCodes['.'] = i;
    sCodesToDigit[i] = '.';
    // sDigitToCodes[' '] = i;
    // sCodesToDigit[i] = ' ';

    digitCodeMappingInitiaized = 1;
}

double *sMusicalToneFactors = NULL;
double *sMusicalToneFreqs = NULL;


void initializeEDCDigitCodeMapping() {
    if (digitCodeMappingInitiaized)
        return;

    int ii;
    for (ii = 0; ii < 256; ii++) {
        sDigitToCodes[ii] = -1;
        sCodesToDigit[ii] = '\0';
    }

    char i = 0;
    sDigitToCodes['\0'] = i;
    sCodesToDigit[i] = '\0';

    char c;
    for (c = '0'; c <= '9'; c++) {
        i++;
        sDigitToCodes[c] = i;
        sCodesToDigit[i] = c;
    }
    for (c = 'A'; c <= 'D'; c++) {
        i++;
        sDigitToCodes[c] = i;
        sCodesToDigit[i] = c;
    }

    i++;
    sDigitToCodes['.'] = i;
    sCodesToDigit[i] = '.';

    i++;
    sDigitToCodes[' '] = i;
    sCodesToDigit[i] = ' ';

    digitCodeMappingInitiaized = 1;
}

char switchLeft_c(char *in, int inLength, char addRight) {  //Plane string decompress(20 byte)
    int mask = 0xFC;
    char out = (char)(((in[0] & mask) >> 2) & 0xFF);

    int i;
    for (i = 0; i < inLength; i++) {
        if (i + 1 < inLength) {
            in[i] = (char)((in[i] << 6) | (((in[i + 1] & mask) >> 2) & 0xFF));
        } else {
            in[i] = (char)((in[i] << 6) | addRight);
        }
    }

    return out;
}

char switchDigitLeft(char *in, int inLength, char addRight) //Digit decompress (30byte)
{
    int mask = 0xF0;
    char out = (char)(((in[0] & mask) >> 4) & 0xFF);

    int i = 0;
    for (i = 0; i < inLength; i++) {
        if (i + 1 < inLength) {
            in[i] = (char)((in[i] << 4) | (((in[i + 1] & mask) >> 4) & 0xFF));
        } else {
            in[i] = (char)((in[i] << 4) | addRight);
        }
    }

    return out;
}

char *decompressPlainStringNative_c(char *d,char *out) {
    int i;
    initializeCharCodeMapping();

    int len=15;
    char data[15];
    for(i=0;i<15;i++){data[i]=d[i];}

    for (i = 0; i < 21; i++) {
        out[i] = '\0';
    }

    for (i = 0; i < 20; i++) {
        char b = switchLeft_c(data, len, (char) 0);
        char c = sCodesToChars[b];

        if (c == '\0') {
            break;
        }

        out[i] = c;
    }

    return out;

}
char *decompressDigitsNative(char *d, char * out, int outLength)
{
    int i=0,len=0;
    len=16;
    char data[len];
    initializeDigitCodeMapping();

    for(i=0;i<len;i++)
    {
        data[i]=d[i];
    }
    // char out[outLength + 1];
    for (i = 0; i < outLength + 1; i++) {
        out[i] = '\0';
    }

    //int i;
    for (i = 0; i < outLength; i++) {
        char b = switchDigitLeft(data, len, (char) 0);
        char c = sCodesToDigit[b];

        if (c == '\0' ) {
            break;
        }

        out[i] = c;
    }

    //jstring outString = (*env)->NewStringUTF(env, (const char *) out);
    //free(out);
    //(*env)->ReleaseByteArrayElements(env, d, data, 0);

    return out;
}

char *decompressEDCDigitsNative(char d[],char out[],int outLength) {
    initializeEDCDigitCodeMapping();

    int i=0;
    int len=16;
    char data[16];
    // jbyte *data = (*env)->GetByteArrayElements(env, d, 0);
    // jsize len = (*env)->GetArrayLength(env, d);

    for(i=0;i<16;i++){data[i]=d[i];}

    // char *out = malloc(outLength + 1);
    for (i = 0; i < outLength + 1; i++) {
        out[i] = '\0';
    }


    for (i = 0; i < outLength; i++) {
        char b = switchDigitLeft(data, len, (char) 0);
        char c = sCodesToDigit[b];

        if (c == '\0' || c == ' ') {
            break;
        }

        out[i] = c;
    }

    // jstring outString = (*env)->NewStringUTF(env, (const char *) out);

    //  free(out);

    //   (*env)->ReleaseByteArrayElements(env, d, data, 0);

    return out;
}

static unsigned char * CRC16native_c(unsigned char data[],unsigned char outarray[],int n) {
    int len=n;
    char bt;
    int wCrc = 0x6363;
    int idx = 0;

    do {
        bt = data[idx];
        idx++;

        bt = (char)(((bt & (0xFF)) ^ (wCrc & 0xFF)) & (0xFF));
        bt = (char)(((bt & (0xFF)) ^ ((bt << 4) & (0xFF))) & (0xFF));
        wCrc = (((wCrc) >> 8) & (0x00FFFFFF)) ^ ((bt & (0xFF)) << 8) ^ ((bt & (0xFF)) << 3) ^
        (((bt & (0xFF)) >> 4) & (0x0FFFFFFF));
    } while (idx < len);

    outarray[0] = (char)(wCrc & 0xFF);
    outarray[1] = (char)((wCrc >> 8) & 0xFF);
    return outarray;
}


//////////////////////////////////////UTILS END///////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////EXPIRY STARTS///////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////EXPIRY ENDS/////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////SONIC PLAYER STARTS/////////////////////////////////////////////////////////////////////////////////
///////////////////Sonic 30byte player///////////////////////////////////////////
#define S_RATE  (44100)
#define NUM_SAMPLES 512*18+512*7*40
#define BUF_SIZE (NUM_SAMPLES*2)

#define inputStringLength 20
#define bufferSize 15
#define crcBytesBuffer 2
#define stringAndCRCBytesBuffer 16
#define stringAndOuterCRCBytesBuffer 18
#define bytesCount 20
#define bitquadLength 40
#define freqArrayLength 40

//int freqsToSend[freqArrayLength];
int usedFreq[40];
int usedFreqIndex_cnd = 0;
int tempArray[40];
int freqOffset=210;
static char charArray[64] = {'\0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9',' '};
static unsigned char byteArray[64] = {0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xA,0xB,0xC,0xD,0xE,0xF,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F};
static unsigned char SECRET_KEY_AES[] = {0xA4, 0x83, 0x54, 0x6F, 0x38, 0xC5, 0xA6, 0x6A, 0x50, 0x11, 0x7C, 0xF2, 0xB0, 0xDD, 0x31, 0xDC};

unsigned char comp[15];
unsigned char crcBytes[2];
unsigned char buffer[BUF_SIZE*2];

int mRetailerMode = 1;
int digitCodeMappingInitiaized;

///////////////////////////////////////////////////////////////////////


unsigned char switchLeft(unsigned char in[], unsigned char addRight)
{
				int i;
    unsigned char mask = 0xFC;
    unsigned char out = (unsigned char)(  ((in[0] & mask) >> 2)&0xFF );


    for(i=0; i < 15; i++)
    {
								if (i+1 < 15)
                                {
                                    in[i] = (unsigned char)((in[i] << 6) | (  ((in[i+1] & mask) >> 2)&0xFF ));
                                }else{
                                    in[i] = (unsigned char)((in[i] << 6) | addRight);
                                }
    }

    return out;
}

unsigned char getByte(char c, char storedCharArray[], unsigned char storedByteArray[]){

    int index = 0;
				int j;

    for (j = 0; j < 64; j++) {
        if(c == storedCharArray[j]){
            index = j;
            break;
        }
    }

    return storedByteArray[index];
}

unsigned char * compString(char s[]){

    int i;

    for(i=0; i<20; i++){
        char c;
	       if(i < inputStringLength){
               c = s[i];
           }else{
               c = '\0';
           }

        unsigned char b = getByte(c, charArray, byteArray);

        switchLeft(comp, b);

	   }

    return comp;

}

unsigned char * compressDigitsNative(char data[],char outjarray[],int length)
{

    initializeDigitCodeMapping();

    char out[length];

    int i;
    for (i = 0; i < (length * 2); i++) {

        char c;

        if (i < strlen(data)) {
            c = data[i];
        } else {
            c = '\0';
        }

        char code = sDigitToCodes[c];

        if (code == -1) {

            code = sDigitToCodes[' '];
        }

        switchDigitLeft(out, length, code);
    }

    memset(outjarray,'\0',(int)sizeof((char*)outjarray));
    for(i=0;i<length;i++)
    {
        outjarray[i]=out[i];
    }

    return outjarray;

}


void initializeFerqArray(){
    int i;
    for(i = 0; i < 40; i++){
        freqsToSend[i] = 0;
        usedFreq[i] = 0;
        tempArray[i] = 0;
    }

    memset(comp,'\0',sizeof(comp));
    memset(crcBytes,'\0',sizeof(crcBytes));
    memset(buffer,'\0',sizeof(buffer));
}


int usedFreqContains(int f){
    int rt = 0;
    int x;
    for(x =0; x < usedFreqIndex_cnd; x++){
        if(usedFreq[x] == f){
            rt = 1;
            break;
        }
        else{
            rt = 0;
        }
    }

    return rt;

}

void usedFreqAdd(int f){

    usedFreq[usedFreqIndex_cnd] = f;
    usedFreqIndex_cnd++;

}

void usedFreqRemove(int idx){

    int l;
    int m = 0;
    int n;

    for(l =0; l < usedFreqIndex_cnd; l++){
        if(l == idx){

        }else{
            tempArray[m] = usedFreq[l];
            m++;
        }
    }

    usedFreqIndex_cnd--;

    for(n = 0; n < usedFreqIndex_cnd; n++){
        usedFreq[n] = tempArray[n];
    }

}

void addFreq(unsigned char *buf, int offset, int sizeSamples, int freq, int amplitude){
    int i;
    for(i = 0; i < sizeSamples; i++){
        float sineWidth = sizeSamples / (float)freq;
        short int val = (short int)(sin(i*(2*M_PI)/sineWidth)*amplitude);
        //buffer[i]=(short int)(sin(i*(2*M_PI)/sineWidth)*amplitude);
        short int oldVal=(short int)((buffer[offset*2+i*2]&0x000000ff)|((buffer[offset*2+i*2+1]&0x000000ff)<<8));

        val += oldVal;

        buffer[offset*2+i*2] = (unsigned char)(val & 0x00FF);
        buffer[offset*2+i*2+1] = (unsigned char)((val & 0xFF00) >> 8);
    }
}

void genTone(int sendingFreq[]){
    int vol = 32000;
    int i,j;
    int kt;

    for(i = 12; i< 36; i++){
        addFreq(buffer, 256*(i-12), 256, 43-(i-12), vol);
    }

    int mainOffset = 256 * 24 + 256 * 32;

    for (kt = 0; kt < freqArrayLength; kt++) {      /// freqArrayLength = 40

        int toneOffset;

        toneOffset = mainOffset + 256 * (7 * kt);

        for (j = 0; j < 7; j++) {
            addFreq(buffer, toneOffset + j * 256, 256, sendingFreq[kt], vol);
        }

    }

}


char *playSonic30Byte(char data[],char bufferRet[],int size1)
{
    initializeFerqArray();

    unsigned char *stringBytes, *stringBytesCRC,*stringAndCRCBytesAES;
    unsigned char stringAndCRCBytes[bytesCount-4],stringAndCRCBytes1[bytesCount-4];
    unsigned char bytesToSend[bytesCount],SECRET_KEY_AES1[16],temp[15+1]={0};
    int i=0,n=0;
   // printf("str=%s\n",data);
    //stringBytes = compString(str);
    stringBytes = compressDigitsNative(data,temp,15);

#ifdef DEV_DEBUG_L1
    printf("comresssedString stringBytes\n");
    n=strlen(stringBytes);
    printf("stringByte size = %d\n",n);
    for(i=0;i<strlen(stringBytes);i++)
    {
        printf("%x ",stringBytes[i]);
    }
    printf("\n");
#endif

    char outarray[2]={0};
    stringBytesCRC = CRC16native_c(stringBytes,outarray,15);

#ifdef DEV_DEBUG_L1
    printf("crc16 str\n");
    n=strlen(stringBytesCRC);
    printf("stringByteCRC size = %d\n",n);
    for(i=0;i<strlen(stringBytesCRC);i++)
    {
        printf("%x ",stringBytesCRC[i]);
    }
    printf("\n");
#endif


    for(i =0; i<15; i++)
    {
        stringAndCRCBytes[i] = stringBytes[i];

    }

#ifdef DEBUG
    printf("coppy stringBytes to stringAndCRCBytes\n");
    n=strlen(stringAndCRCBytes);
    printf("stringAndCRCBytes size = %d\n",n);
    for(i=0;i<strlen(stringAndCRCBytes);i++)
    {
        printf("%x ",stringAndCRCBytes[i]);
    }
    printf("\n");
#endif


    for (i = 0; i < 1; i++) {
        stringAndCRCBytes[15] = (unsigned char) ((stringBytesCRC[0] & 0xFF) + (stringBytesCRC[1] & 0xFF));
    }

#ifdef DEBUG
    printf("combine 2byte o/p of crc into 1byte in stringAndCRCBytes[15]\n");
    for(i=0;i<16;i++)
    {
        printf("%x ",stringAndCRCBytes[i]&0xff);
    }
    printf("\n");
#endif


    memcpy(stringAndCRCBytes1,stringAndCRCBytes,sizeof(stringAndCRCBytes));
    memcpy(SECRET_KEY_AES1,SECRET_KEY_AES,sizeof(SECRET_KEY_AES));
    char aes_temp[16+1]={0};
    stringAndCRCBytesAES=AESnative_c(stringAndCRCBytes1,SECRET_KEY_AES1,0,aes_temp);

#ifdef DEBUG
    printf("AES encrypted cipher text\n");
    for (i=0;i<16;i++) {

        printf("%x ",aes_temp[i]&0xff);
    }
    printf("\n");
#endif
    memset(bytesToSend,'\0',sizeof(bytesToSend));
    for(i=0; i<2; i++)
    {
        if (mRetailerMode) {
            bytesToSend[i] = 4 + 64;
            //   bytesToSend[f] = 2 + 32;
            //  bytesToSend[f] = 1 + 16;
        } else {
            bytesToSend[i] = 1 + 16;
        }
    }

#ifdef DEBUG
    printf("bytesToSend 1st 2byte\n");
    for(i=0;i<2;i++)
    {
        printf("%x ",bytesToSend[i]&0xff);
    }
    printf("\n");
#endif


    for (i = 2; i < bytesCount - 2; i++) {
        bytesToSend[i] = aes_temp[i - 2];
    }

#ifdef DEBUG
    printf("bytesToSend full\n");
    for(i=0;i<bytesCount;i++)
    {
        printf("%x ",bytesToSend[i]&0xff);
    }
    printf("\n");
#endif

    unsigned char *crc_16,outarray1[2]={0};

    crc_16 = CRC16native_c(aes_temp,outarray1,16);

#ifdef DEBUG
    printf("2nd crc16\n");
    for(i=0;i<2;i++)
    {
        printf("%x ",crc_16[i]&0xff);
    }
    printf("\n");
#endif

    for (i = bytesCount - 2; i < bytesCount; i++) {
        bytesToSend[i] = crc_16[i - (bytesCount - 2)];
    }

#ifdef DEV_DEBUG_L1
    printf("bytesToSend full\n");
    for(i=0;i<bytesCount;i++)
    {
        printf("%x ",bytesToSend[i]&0xff);
    }
    printf("\n");
#endif


    int bitQuads[bytesCount * 2];
    for (i=0; i<bytesCount*2; i++)
    {
        unsigned char byteToSend = bytesToSend[i/2];
        int halfByteToSend;

        if (i%2==0)
        {
            halfByteToSend = ((byteToSend & 0x000000F0) >> 4);
        }
        else
        {
            halfByteToSend = (byteToSend & 0x0000000F);
        }
        bitQuads[i] = halfByteToSend;
    }

#ifdef DEBUG
    printf("bitQuads \n");
    for(i=0;i<bytesCount*2;i++)
    {
        printf("%d ",bitQuads[i]);
    }
    printf("\n");
#endif


  //  int freqsToSend[bytesCount * 2];

    for(i =0; i< bytesCount*2; i++){
        int bitQuad = bitQuads[i];

        int freeCount = 0;
        int fc;
        for(fc=12; fc<36; fc++){
            if (usedFreqContains(fc) == 0)
            {
                freeCount++;
            }
            if (freeCount-1 == bitQuad)
            {
                freqsToSend[i] = fc;
                usedFreqAdd(fc);

                if (usedFreqIndex_cnd > 8)
                {
                    usedFreqRemove(0);
                }
                break;
            }


        }

    }


#ifdef DEBUG
    printf("freqsToSend.. \n");
    for(i=0;i<40;i++)
    {
        printf("%d ",freqsToSend[i]&0xff);
    }
    printf("\n");
#endif

  /*  genTone(freqsToSend);            // audible

    for(i=0;i<size;i++)
    {
        bufferRet[i]=buffer[i];
    }
    bufferRet[++i]='\0';*/

    return bufferRet;
}
///////////////////Sonic 30byte player end///////////////////////////////////////

//unsigned char* AESnative_c(unsigned char *data, unsigned char *key, unsigned char dir,unsigned char *state);
/////////////////////////////////////////////////// SOUND RECORDER STARTS//////////////////////////////////////////////////////////////////////

///////////////////////////////////Normal tone Recorder//////////////////////////////////////////////

#define FFT_BUFFER_LENGTH 256
#define FREQUENCY_COUNT 32
#define FREQUENCY_LOWEST 12
#define FREQUENCY_HIGHEST 43

int mStringRecording;
int mBytesRecording;
int mAmountRecording;
int mStatusRecording;
int m30BytesRecording;
int otpEncrypted;

int listenFrom;
int mAntiDuplicationMode;

int mFramesCount;

//int FREQUENCY_COUNT = 32;
int TONE_LENGTH = 7;

double mFreqStream[7 * 2 * 20 * 2][32];
int mFreqStreamLength;
int mFreqStreamPointer;

double mFreqsMaxArray[24][2];

int mBitQuads[(16 * 3 + 4) * 2];

int mUsedFreqs[8];
int mUsedFreqsPointer;

int AVERAGE_VOLUME_COUNT = 11;
int FFT_VOLUME_MULTIPLIER = 32;

short mVolumeThreshold = -1;


int strongestFrequencyVolume;


double *cosArr_normal;
double *sinArr_normal;
int m_normal;
int n_normal;

void Java_com_tonetag_tone_FFT_initFFTNormal(int fftn) {
    n_normal = fftn;
    m_normal = (int) (log(n_normal) / log(2));

    // precompute tables
    cosArr_normal = malloc((n_normal / 2) * sizeof(double));
    sinArr_normal = malloc((n_normal / 2) * sizeof(double));

    int i;
    for (i = 0; i < n_normal / 2; i++) {
        cosArr_normal[i] = cos(-2 * M_PI * i / n_normal);
        sinArr_normal[i] = sin(-2 * M_PI * i / n_normal);
    }
}

void FFT_FFTNormal(int n)
{
    Java_com_tonetag_tone_FFT_initFFTNormal(n);
}

void FFTNormal(double *x, double *y)
{
    //  jsize len = (*env)->GetArrayLength(env, inputX);

    //jdouble *x = (*env)->GetDoubleArrayElements(env, inputX, 0);
    //jdouble *y = (*env)->GetDoubleArrayElements(env, inputY, 0);

    int i, j, k, n1, n2, a;
    double c, s, t1, t2;

    // Bit-reverse
    j = 0;
    n2 = n_normal / 2;
    for (i = 1; i < n_normal - 1; i++) {
        n1 = n2;
        while (j >= n1) {
            j = j - n1;
            n1 = n1 / 2;
        }
        j = j + n1;

        if (i < j) {
            t1 = x[i];
            x[i] = x[j];
            x[j] = t1;
            t1 = y[i];
            y[i] = y[j];
            y[j] = t1;
        }
    }

    // FFT
    n1 = 0;
    n2 = 1;

    for (i = 0; i < m_normal; i++) {
        n1 = n2;
        n2 = n2 + n2;
        a = 0;

        for (j = 0; j < n1; j++) {
            c = cosArr_normal[a];
            s = sinArr_normal[a];
            a += 1 << (m_normal - i - 1);

            for (k = j; k < n_normal; k = k + n2) {
                t1 = c * x[k + n1] - s * y[k + n1];
                t2 = s * x[k + n1] + c * y[k + n1];
                x[k + n1] = x[k] - t1;
                y[k + n1] = y[k] - t2;
                x[k] = x[k] + t1;
                y[k] = y[k] + t2;
            }
        }
    }


    //   (*env)->ReleaseDoubleArrayElements(env, inputX, x, 0);
    //   (*env)->ReleaseDoubleArrayElements(env, inputY, y, 0);

}

void useFreq(int freq) {
    mUsedFreqsPointer++;
    mUsedFreqsPointer %= 8;
    mUsedFreqs[mUsedFreqsPointer] = freq;
}

void resetRecordMode() {
    mStringRecording = 0;
    mBytesRecording = 0;
    mAmountRecording = 0;
    mStatusRecording = 0;
    m30BytesRecording = 0;
    loopbreak=1;
}
int getStrongestFreq(double *freqs) {
    int res = -1;
    int i;
    // Last 8 frequencies are used for anti-duplication
    for (i = 0; i < FREQUENCY_COUNT - 8; i++) {

        int contains = 0;

        int j;
        for (j = 0; j < 8; j++) {
            if (mUsedFreqs[j] == i) {
                contains = 1;
                break;
            }
        }

        if (contains)
            continue;

        if (res == -1 || freqs[i] * mFreqsMaxArray[res][0] > freqs[res] * mFreqsMaxArray[i][0]) {
            res = i;

            strongestFrequencyVolume = freqs[i];
        }
    }

    strongestFrequencyVolume /= AVERAGE_VOLUME_COUNT;
    strongestFrequencyVolume /= FFT_VOLUME_MULTIPLIER;

    return res;
}


int getBitQuad(double *freqs) {

    int i;
    int freq = getStrongestFreq(freqs);

    int freeCount = 0;
    for (i = 0; i <= freq; i++) {

        int contains = 0;

        int j;
        for (j = 0; j < 8; j++) {
            if (mUsedFreqs[j] == i) {
                contains = 1;
                break;
            }
        }

        if (!contains) {
            freeCount++;
        }

    }

    useFreq(freq);

    return freeCount - 1;
}

int *bitQuadTofreqs(int bitQuads[],int freqsToSend[])
{
   initializeFerqArray();
   //int freqsToSend[bytesCount * 2];
    int i=0;
/*
    printf("\nInside bitQuadTofreqs\n");
    for(i=0;i<40;i++){printf(" %d",bitQuads[i]);}
    printf("\n");
*/
    for(i =0; i< bytesCount*2; i++){
        int bitQuad = bitQuads[i];

        int freeCount = 0;
        int fc;
        for(fc=12; fc<36; fc++){
            if (usedFreqContains(fc) == 0)
            {
                freeCount++;
            }
            if (freeCount-1 == bitQuad)
            {
                freqsToSend[i] = fc;
                usedFreqAdd(fc);

                if (usedFreqIndex_cnd > 8)
                {
                    usedFreqRemove(0);
                }
                break;
            }


        }

    }
 return freqsToSend;
}

int decode_done=0;		//to be used by paRecPlay.c

void processStream() {

    int i;
#ifdef DEBUG
    printf("processStream30\n");
#endif

    for (i = 0; i < 8; i++) {
        mUsedFreqs[i] = -1;
    }

    mUsedFreqsPointer = 0;

    double averageVolume = 0;

    strongestFrequencyVolume = 0;

    for (i = 0; i < mFreqStreamLength; i += TONE_LENGTH * 2) {

        int pos = mFreqStreamPointer + 1 + i;
        pos %= mFreqStreamLength;

        int bitQuad = getBitQuad(mFreqStream[pos]);

        averageVolume += strongestFrequencyVolume;


        if(listenFrom != SENDER_ALL) {
            if (listenFrom == SENDER_CUSTOMER) {//|| (listenFrom == allFieldValue)) {
                if (i / (TONE_LENGTH * 2) < 4 && bitQuad != 1) {
                    return;
                }
            }
            if (listenFrom == SENDER_RETAILER) {// || (listenFrom == allFieldValue)) {
                if (i / (TONE_LENGTH * 2) < 4 && bitQuad != 4) {
                    return;
                }
            }
            if (listenFrom == SENDER_POS) {// || (listenFrom == allFieldValue)) {
                if (i / (TONE_LENGTH * 2) < 4 && bitQuad != 2) {
                    return;
                }
            }
            if(listenFrom == SENDER_EDC){
                if (i / (TONE_LENGTH * 2) < 4 && bitQuad != 6) {
                    return;
                }
            }
        }else{

            if (i / (TONE_LENGTH * 2) < 4 && bitQuad != 4){ //&& ((bitQuad != 1)||(bitQuad != 2)||(bitQuad != 4))) {
                return;
            }
        }

        mBitQuads[i / (TONE_LENGTH * 2)] = bitQuad;
    }

toneDetected=1;

//#define DEBUG
#ifdef DEBUG
	printf("BitQuads\n");
	for(i=0;i<40;i++) {printf(" %d",mBitQuads[i]);}
	printf("\n");
#endif

//....................................POD..................................................//
/*
   int frqToSend1[40+1],*frqToSend2;
  frqToSend2=bitQuadTofreqs(mBitQuads,frqToSend1);
  checkIndexMatch(recvdindex,frqToSend2);
*/
//....................................POD end..............................................//

    averageVolume /= (mFreqStreamLength / (TONE_LENGTH * 2));



    const int outLength = (20 * 2) / 2 - 4; //outLength=16
    char out[outLength];
    memset(out,'\0',sizeof(out));

    for (i = 0; i < outLength; i++) {
        int high = mBitQuads[4 + i * 2];
        int low = mBitQuads[4 + i * 2 + 1];

        out[i] = (char)((high << 4) | low);
    }


    char crc16original[2]={0,0};

    for (i = 0; i < 2; i++) {
        int high = mBitQuads[4 + outLength * 2 + i * 2];
        int low = mBitQuads[4 + outLength * 2 + i * 2 + 1];

        crc16original[i] = (char)((high << 4) | low);
    }


    //  jarray jout = (*env)->NewByteArray(env, outLength);
    //  (*env)->SetByteArrayRegion(env, jout, 0, outLength, out);
    char jout[outLength+1];
    memset(jout,'\0',sizeof(jout));

    for(i=0;i<16;i++){jout[i]=out[i];}

#ifdef DEBUG
    printf("jout..\n");
    for(i=0;i<16;i++) {printf(" %x",jout[i]&0xff);}
    printf("\n");
#endif

    // jarray crc16calculated = CRC16native(env, cls, jout);
    // jbyte *crc16calculatedC = (*env)->GetByteArrayElements(env, crc16calculated, 0);
    char outarray[2] = {0, 0};
    char * crc16calculated = CRC16native_c(out,outarray,16);

    int crcMatch = 1;


    for (i = 0; i < 2; i++) {
        if (crc16original[i] != crc16calculated[i]) {
            crcMatch = 0;
        }
    }

#ifdef DEBUG
    printf("crc16calculated\n");
    for(i=0;i<2;i++) {printf(" %x",crc16calculated[i]&0xff);}
    printf("\n");
#endif

#ifdef DEBUG
    printf("crc16original\n");
    for(i=0;i<2;i++) {printf(" %x",crc16original[i]&0xff);}
    printf("\n");
#endif

    FILE *fd;//,*fd1;
    unsigned char decryptedOutC[16];
    unsigned char *decryptedOut;
    unsigned char aes_temp[16];
    memset(aes_temp,'\0',sizeof(aes_temp));
    memset(decryptedOutC,'\0',sizeof(decryptedOutC));

    if (crcMatch) {

        decryptedOut = (unsigned char *)AESnative_c(jout,key1,1,aes_temp);

        for(i=0;i<16;i++) {decryptedOutC[i]=aes_temp[i];}


#ifdef DEBUG
        printf("decryptedOutC\n");
   	    for(i=0;i<16;i++) {printf(" %x",decryptedOutC[i]&0xff);}
   	    printf("\n");
#endif

        int identifier = 4;
        identifier=0;
        for (i = 0; i < 2; i++) {
            int high = mBitQuads[i * 2];
            int low = mBitQuads[i * 2 + 1];

            if(high == 1 && low == 1){
                identifier = 4;
                // identifier1 = 4;
            }else if(high == 2 && low == 2){
                identifier = 6;
                // identifier1 = 6;
            }else if(high == 4 && low == 4){
                identifier = 5;
                // identifier1 = 5;
            }else if(high == 6 && low == 6){
                identifier = 8;
                // identifier1 = 8;
            }
        }

//        int duplicated = 0;


        int innerCRCmatch = 1;

        char data[15+1]={0};
        memset(data,'\0',sizeof(data));

        char innerOrigCRC[1+1]={0};
        //char *innerCalcCRC;

        for (i = 0; i < 15; i++) {
            data[i] = decryptedOutC[i];
        }

        for (i = 0; i < 1; i++) {
            innerOrigCRC[i] = decryptedOutC[15 + i];
        }



        char jdata[15+1]={0};
        memset(jdata,'\0',sizeof(jdata));
        for(i=0;i<15;i++){jdata[i]=data[i];}

        char outarray[2+1] = {0, 0};
        char innerCalcCRCC[2+1]={0, 0};
        char * innerCalcCRC = CRC16native_c(out,outarray,16);

        if (innerOrigCRC[0] !=
            (char)((innerCalcCRCC[0] & 0xFF) + (innerCalcCRCC[1] & 0xFF))) {
            innerCRCmatch = 0;
        }



        //  if (innerCRCmatch)


            char *outString;


            outString = decompressDigitsNative(decryptedOutC,decoded_string, 32);//decompressPlainStringNative(env, cls, jdata);

            memset(gData,'0',sizeof(gData));
            strcpy(gData,decoded_string);
            if(testcase==SPEAKER){
            printf("Normal_30..=%s\n",decoded_string);
            decode_done=1;
            }

            resetRecordMode();
	//....................................POD Detect..................................................//
			//static int toneDecripted=0;
/*
            toneDecripted++;      //data detected in tone

			fd=fopen(output,"a+");
			if(fd==NULL){printf("File not opened After crc\n");}
			printf("%s\n","DATA RECIEVED");
			fprintf(fd,"%s\n","DATA RECIEVED");
			fprintf(fd,"%s\n",gData);
			fprintf(fd,"Rcvd %d\n",toneDecripted);
			printf("Rcvd %d\n",toneDecripted);
			fclose(fd);
*/
			/*fd1=fopen("recvdCount.txt","w");
			if(fd1==NULL){printf("File not opened\n");}
			fprintf(fd1," %d",toneDecripted);
			fclose(fd1);*/


	//....................................POD Detect end.............................................//


    }
}

double *freqsFromPosition(int position) {
    position += mFreqStreamLength;
    position %= mFreqStreamLength;

   //  printf("freqsFromPosition:%d\n",position);
    return mFreqStream[position];
}

void processFreqsNative(double *freqs)
{


    int i;

    mFreqStreamPointer++;
    mFreqStreamPointer = mFreqStreamPointer % mFreqStreamLength;

    double *newFreqs = freqsFromPosition(mFreqStreamPointer);

    for (i = 0; i < FREQUENCY_COUNT; i++) {
        newFreqs[i] = freqs[i];
        int j;
        for (j = 2; j <= TONE_LENGTH * 2 - 3; j++) {
            freqsFromPosition(mFreqStreamLength - j)[i] += newFreqs[i];
        }
    }

    mFramesCount++;

    if (mFramesCount > 100)
        for (i = 0; i < FREQUENCY_COUNT - 8; i++) {
            if (freqs[i] > mFreqsMaxArray[i][0]) {
                mFreqsMaxArray[i][0] = freqs[i];
            }

            if (freqs[i] > mFreqsMaxArray[i][1]) {
                mFreqsMaxArray[i][1] = freqs[i];
            }
        }


    int soundTagLengthFrames = (TONE_LENGTH * 2) * (20 * 2) + 140;

    if (mFramesCount % soundTagLengthFrames == 0) {
        for (i = 0; i < FREQUENCY_COUNT - 8; i++) {
            mFreqsMaxArray[i][0] = mFreqsMaxArray[i][1];
            mFreqsMaxArray[i][1] = 0;
        }
    }

#ifdef DEBUG
  //  printf("processFreqsNative\n");
#endif

    if (mStringRecording) {
        processStream();
    } //else if (mBytesRecording) {
    // processStreamBytes(env, obj);
    // } else if (mAmountRecording) {
    // processStreamAmount(env, obj);
    // } else {
    // processStreamStatus(env, obj);
    // }
}

void initRecordingNative(int recordMode, int from,int antiDuplicationMode)
{

    if (recordMode == STRING_RECORD_MODE) {
        mStringRecording = 1;
        otpEncrypted = 0;
    }
    else if (recordMode == _30_BYTES_RECORD_MODE){
        m30BytesRecording = 1;
    }
    else if (recordMode == BYTES_RECORD_MODE) {
        mBytesRecording = 1;
    }
    else if (recordMode == AMOUNT_RECORD_MODE) {
        mAmountRecording = 1;
    } else if (recordMode == STATUS_RECORD_MODE) {
        mStatusRecording = 1;
    } else if (recordMode == TXN_STRING_RECORD_MODE) {
        mStringRecording = 1;
        otpEncrypted = 1;
    }


    listenFrom = from;
    if (mStringRecording == 0) {
        mAntiDuplicationMode = 0;
    } else {
        mAntiDuplicationMode = antiDuplicationMode;
    }
    mFramesCount = 0;

    if (mStringRecording) {
        mFreqStreamLength = TONE_LENGTH * 2 * 20 * 2;
    } else if(m30BytesRecording){
        mFreqStreamLength = TONE_LENGTH * 2 * 20 * 2;
    }else if (mBytesRecording) {
        mFreqStreamLength = TONE_LENGTH * 2 * 12 * 2;
    } else if (mAmountRecording) {
        mFreqStreamLength = TONE_LENGTH * 2 * 8 * 2;
    } else if (mStatusRecording) {
        mFreqStreamLength = TONE_LENGTH * 2 * 8 * 2;
    }

    int i;
    for (i = 0;i < FREQUENCY_COUNT - 8; i++) {
        mFreqsMaxArray[i][0] = 0;		//double mFreqsMaxArray[24][2];
        mFreqsMaxArray[i][1] = 0;
    }

    mFreqStreamPointer = 0;

    for (
         i = 0;
         i < 7 * 2 * 20 * 2; i++) {
        int j;
        for (j = 0;j < 32; j++) {
            mFreqStream[i][j] = 0;
        }
    }

#ifdef DEBUG
    printf("initRecordingNative end\n");
#endif
}

bool mAntiDuplicationMode1;
bool isRecording = false;

short *subData;
short *buf1;
short *buf2;
short *bufMid;
short *bufTmp;

void freeVar()
{
/*
free(buf1);
free(buf2);
*/
/*free(bufMid);
*/
//free(subData);
}

void startNormalRecording(int recordMode, int from, bool antiDuplicationMode,int antiDuplicationVolume,int deviceVolume)
{
    if (recordMode != STRING_RECORD_MODE && recordMode != TXN_STRING_RECORD_MODE) {
        mAntiDuplicationMode1 = false;
    } else {
        mAntiDuplicationMode1 = antiDuplicationMode;
    }


    initRecordingNative(recordMode, from, antiDuplicationMode ? 1 : 0);


    FFT_FFTNormal(FFT_BUFFER_LENGTH); //i


    buf1=(short*)malloc(FFT_BUFFER_LENGTH*sizeof(short));
    buf2=(short*)malloc(FFT_BUFFER_LENGTH*sizeof(short));
    bufMid=(short*)malloc(FFT_BUFFER_LENGTH*sizeof(short));
    subData=(short*)malloc(FFT_BUFFER_LENGTH*sizeof(short));

}


void consumeDataInternal(short *data)
{
	double x[FFT_BUFFER_LENGTH];
	double y[FFT_BUFFER_LENGTH];
	double freqs[FREQUENCY_COUNT];
//printf("consumeDataInternal\n");
    buf1 = buf2;
    buf2 = data;

    int i;
    for (i = FFT_BUFFER_LENGTH / 2; i < FFT_BUFFER_LENGTH; i++) {
        bufMid[i - FFT_BUFFER_LENGTH / 2] = buf1[i];
    }

    for (i = 0; i < FFT_BUFFER_LENGTH / 2; i++) {
        bufMid[i + FFT_BUFFER_LENGTH / 2] = buf2[i];
    }

    for (i = 0; i < FFT_BUFFER_LENGTH; i++) {
        x[i] = bufMid[i];
        y[i] = 0;
    }


    FFTNormal(x, y);

    for (i = 0; i < FREQUENCY_COUNT; i++) {
        int freqIdx = i + FREQUENCY_LOWEST;
        freqs[i] = sqrt((x[freqIdx] * x[freqIdx]) + (y[freqIdx] * y[freqIdx]));
    }


        processFreqsNative(freqs);


    for (i = 0; i < FFT_BUFFER_LENGTH; i++) {
        x[i] = buf2[i];
        y[i] = 0;
    }

    FFTNormal(x, y);


    for (i = 0; i < FREQUENCY_COUNT; i++) {
        int freqIdx = i + FREQUENCY_LOWEST;
        freqs[i] = sqrt((x[freqIdx] * x[freqIdx]) + (y[freqIdx] * y[freqIdx]));
    }
    //processFreqs(freqs);
    //if (mAudioRecord != 0)
    {
        processFreqsNative(freqs);
    }
    //buf1=NULL;
}

void consumeData(short data[]) {
    int i,j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < FFT_BUFFER_LENGTH; j++) {
            subData[j] = data[i * FFT_BUFFER_LENGTH + j];

        }

        consumeDataInternal(subData);
    }
}


///////////////////////////Normal tone Recorder end/////////////////////////////////////////////
/////////////////////////////////////////////////// SOUND RECORDER End///////////////////////////////////////////////////////////


