/*
 * pod_callbacks.h
 *
 *  Created on: 25-Feb-2019
 *      Author: Gaurav
 */

#ifndef POD_CALLBACKS_H_
#define POD_CALLBACKS_H_

#include "Tserial_event.h"
#include "serialmanager.h"
#include "pod_com.h"
#include "com_proto.h"

/********Callbacks structure -- entry **********/


 /********Callbacks structure -- exit **********/

/**********Function prototypes -- entry*****************************/

void pod_status_cb(int status);
void pod_api_status_cb(int API, int status);
void pod_info_recv_cb(unsigned int status, char* Amount);
void pod_tap_status_cb(int status);


void pod_on_event_cb(int status);
void pod_on_data_rx_cb(char*, int);

void ser_on_connect_cb(void);
void ser_on_disconnect_cb(void);
void ser_on_receive_cb(char* rxdata, int stat);
void ser_on_write_cb(int stat);
void ser_on_event_rx_cb(int stat);

/**********Function prototypes -- exit*****************************/

#endif /* POD_CALLBACKS_H_ */
