#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>


#include <windows.h>

#include "sndfile.h"
#include "debugflags.h"		//for enabling/disabling different debug flags
#include "logger.h"
#include "testcases.h"		//for including different use-cases

#include "beacon.h"			//for extern variable str
#include "user_interface.h"

#include "user_errors.h"


//ToneTag header files
#include "libusb.h"				//for using LibUSB in windows
#include "testbench_config.h"	//for configuring the ToneTag POD Testbench

#include "debugflags.h"			//including the debugging flags to the dev source so that a developer can debug with different levels
#include "Tserial_event.h"
#include "serialmanager.h"

#ifdef REPORT_STAT
#include "status_logs.h"		//for reporting the status of different processes to status_logs.c -- the Testing Eng. can monitor these statuses at one place
#endif

#include "log.h"				//using logging the error
#include "pod_com.h"
#include "com_proto.h"

#include "pod_callbacks.h"
#include "errno.h"

#include "usb_node.h"

static int pos_type=1;				//POS type 1 -> FAB, 2 -- HUL
static int stat=0;					//ToneTag status variable -- to be logged in status_logs

int testcase=0;


/**********Interfaces to the POD -- entry***************************/

pos_interface_t *pos_f;		//pointer to POD interface to access function pointers
cmd_interface_t *cmd_f;		//pointer to CMD interface to access function pointers

/**********Interfaces to the POD -- exit***************************/




extern pos_callback_t pos_status_cb;





/*************Custom POS Functions -- entry****************************************/

static int pos_init()			//Initialize the ToneTag POD by getting the handle of POS interface
{
pos_f = get_pos_interface();
return 1;

}

static int cmd_init()
{
cmd_f = get_cmd_interface();
return 1;
}

static int pod_pos_connect()			//for connecting with ToneTag POD -- The OLED display shows CONNECTED after this call
{
pos_f->pos_init(&pos_status_cb, pos_type);
#ifdef USER_DEBUG_L1
printf("Connecting to POD\n");
#endif
WAIT
return 1;
}

 int pod_cmd_toneplay(int devh_index)			//Play the Sonic tone
{

stat=cmd_f->cmd_toneplay(devh_index);
return stat;

}

 int pod_cmd_startMic(int devh_index)			//start the MIC of ToneTag POD
{

stat=cmd_f->cmd_startMic(devh_index);
return stat;

}

int pod_cmd_send_amount(int devh_index)
{
	stat=cmd_f->cmd_send_amount(devh_index);
	return stat;

}

int pod_cmd_send_TID(char* stringTID,int devh_index)
{
	stat=cmd_f->cmd_send_TID(stringTID,devh_index);
	return stat;

}

int pod_cmd_sendserial(int devh_index)
{
	WAIT
	stat=cmd_f->cmd_send_podserial(devh_index);
	return stat;
	WAIT__

}

int pod_cmd_send_wifi_stat(int devh_index)
{
	stat=cmd_f->cmd_send_wifi_stat(devh_index);
	return stat;

}

int pod_led_detect(int devh_index)
{
	stat=cmd_f->cmd_led_detect(devh_index);
	return stat;

}

int pod_get_fw_version(int devh_index)
{
stat=cmd_f->cmd_get_fw_version(devh_index);
return stat;

}

int pod_cmd_send_wifi_stat_stop(int devh_index)
{
	stat=cmd_f->cmd_send_wifi_stat_stop(devh_index);
	return stat;

}

int pod_cmd_start_longpress_detect(int devh_index)
{
	stat=cmd_f->cmd_start_longpress_detect(devh_index);
	return stat;

}

int pod_cmd_stop_longpress_detect(int devh_index)
{
	stat=cmd_f->cmd_stop_longpress_detect(devh_index);
	return stat;

}

int pod_cmd_goto_bootloader(int devh_index)
{
pos_f->system_reset_to_boot(devh_index);
return stat;

}
/*************Custom POS Functions -- exit****************************************/


extern uint8_t last_amount[AMNT_LENGTH];	//Amount fetched by the script from the POD
extern char str[inputStringLength+1];
uint16_t mic_hits,mic_miss;						//number of hits and misses for the test-case involved
char ch[60];							//used for sprintf for generating report logs -- to be managed properly
extern uint16_t speaker_hits,speaker_miss;	//extern speaker hits and miss variables used for speaker report generation

void compare_amount_result(void)
{
#ifdef USER_DEBUG_L1
printf("Amount Received:-%s ",last_amount);
printf("Amount Sent:-%s\n",str);
#endif

int compresult=strcmp(last_amount,str);
if(compresult==0)
{
++mic_hits;
sprintf(ch,"MIC Hits: %d",mic_hits);

LOG(ch,0);
LOG(ch,1);
printf("hit\n");
}
else
{
++mic_miss;
sprintf(ch,"MIC Miss: %d",mic_miss);

LOG(ch,0);
LOG(ch,1);
printf("miss\n");
}

last_amount[AMNT_LENGTH]="";
str[inputStringLength+1]="";

}


#ifdef TESTBENCH_MAIN

#include "timer.h"

int iteration=0;

int pod_handle=-1;		//pod_handle populated by FOR_EVERY_POD in testcases.h and used here
int pod_index=0;		//pod_index populated by FOR_EVERY_POD in testcases.h and used here

char teststart_time[15],testend_time[15];
extern char fail_flag;

extern char FSM_STATE_WIFI,FSM_PREV_STATE_WIFI;

extern int sem;

void speaker_switch(int devh_index);

log_csv_pod pod_log[20];

int main(int argc, char** argv) {



	if(argc<3)
	{
	fprintf(stderr, "%s", arg_err);
	LOG(arg_err,0);
	LOG(arg_err,1);
	exit(0);
	}
	if(argc>3)
	{
	fprintf(stderr, "%s", arg_err);
	LOG(arg_err,0);
	LOG(arg_err,1);
	exit(1);

	}
	 testcase=atoi(argv[1]);
	 iteration=atoi(argv[2]);

#ifdef TESTBENCH_EN


		stat=cmd_init();			//get CMD interface


		char reportheader[]="POD Serial number,SPEAKER TEST,WIFI TEST,MIC TEST,START TIME,END TIME,FINAL RESULT";
		if(checkfile_empty()==1 || checkfile_empty()==2 ){
			//printf(" csv file is empty,insert the first header row\n");
			LOG_excelfmt(reportheader,1);
		}
		else{
			//printf("csv file is not empty, do not insert header\n");
		}


		LOG("Starting Testbench",1);
		LOG("Report generating",0);

#ifdef USER_INTERFACE_EN


		stat=pos_init();			//get POS interface
		pod_pos_connect();		//Connect the POD interface of USB CDC

/*
		int pod_ind=0;

		strcpy(pod_log[pod_ind].pod_serial,"bbe0cc19b03f8b5c");
		strcpy(pod_log[pod_ind].speaker_test,"PASS");
		strcpy(pod_log[pod_ind].wifi_test,"PASS");
		strcpy(pod_log[pod_ind].mic_test,"PASS");
		strcpy(pod_log[pod_ind].start_time,"15:54:00");
		strcpy(pod_log[pod_ind].end_time,"15:55:00");
		strcpy(pod_log[pod_ind].final_result,"PASS");

		 pod_ind=1;

		strcpy(pod_log[pod_ind].pod_serial,"dde0cc19b03f8b5c");
		strcpy(pod_log[pod_ind].speaker_test,"FAIL");
		strcpy(pod_log[pod_ind].wifi_test,"FAIL");
		strcpy(pod_log[pod_ind].mic_test,"PASS");
		strcpy(pod_log[pod_ind].start_time,"12:19:00");
		strcpy(pod_log[pod_ind].end_time,"12:20:00");
		strcpy(pod_log[pod_ind].final_result,"FAIL");

		pod_ind=2;

		strcpy(pod_log[pod_ind].pod_serial,"eee0cc19b03f8b5c");
		strcpy(pod_log[pod_ind].speaker_test,"PASS");
		strcpy(pod_log[pod_ind].wifi_test,"PASS");
		strcpy(pod_log[pod_ind].mic_test,"PASS");
		strcpy(pod_log[pod_ind].start_time,"12:20:00");
		strcpy(pod_log[pod_ind].end_time,"12:21:00");
		strcpy(pod_log[pod_ind].final_result,"PASS");
		LOG_podstruct(&pod_log,1);
*/
		if(testcase == 9)
		{
			FOR_EVERY_POD
			printf("start flashing the POD\n");
		#ifdef POD_FLASH_EN
				pod_cmd_goto_bootloader(pod_handle);
		#endif
			END
				WAIT__
				WAIT__
#ifdef POD_FLASH_EN
				system("bin\\multipleflash.exe");
#endif


		}

		else
		{



		FOR_EVERY_POD
		DATEFILE_CURRENT();
		strcpy(teststart_time,current_time());
		mic_hits=mic_miss=speaker_hits=speaker_miss=0;
		strcpy(pod_log[pod_index].start_time,teststart_time);

#ifdef SEND_SERIAL_EN
		pod_cmd_sendserial(pod_handle);
#endif

#ifdef SPEAKER_TEST_EN
		speaker_switch(pod_handle);
#endif


		END




#ifdef MIC_TEST_EN
		FOR_EVERY_POD

		pod_cmd_startMic(pod_handle);


		END

		mic_switch(pod_handle);
#endif


#ifdef WIFI_TEST_EN
		FOR_EVERY_POD


		wifi_switch(pod_handle);

		END
#endif
		LOG_podstruct(&pod_log,1);
		}


while(1){}
#endif



#endif


}

extern char fail_flag;

void mic_switch(int devh_index)
{


		testcase=1;
		mic_test_screen();
		mic_test(iteration,devh_index);

}

void speaker_switch(int devh_index)
{		testcase=2;
		speaker_test_screen();
		speaker_test(iteration,devh_index);
		sprintf(ch,"SPEAKER TEST REPORT -- No.of Tests: %d  Success: %d Failure: %d",speaker_hits+speaker_miss,speaker_hits,speaker_miss);
		LOG(ch,0);
		LOG(ch,1);
		LOG_excelfmt_start(ch,1);
		if((speaker_hits+speaker_miss) == speaker_hits && speaker_hits>0)
			{
//				LOG_excelfmt("PASS",SPEAKER);

			strcpy(pod_log[pod_index].speaker_test,"PASS");

				//Test Pass
			}
			else{

				strcpy(pod_log[pod_index].speaker_test,"FAIL");
				//Test Fail
//				LOG_excelfmt("FAIL",SPEAKER);
				fail_flag=1;

			}
		memset(ch,"\0",strlen(ch));
		sprintf(ch,"%d,%d,%d",speaker_hits,speaker_miss,(speaker_hits/(speaker_hits+speaker_miss))*100);
		//LOG_excelfmt(ch,"SPEAKER");
		//exit(0);

}


extern int timer_testcase;
void wifi_switch(int devh_handle)
{




testcase=7;
wifi_test_screen();
pod_cmd_send_wifi_stat(devh_handle);		//1 is temporarily passed for testing  -- have to modified to a dynamic method

/*
start_timer_1sec();

while(timer_testcase<WIFI_TIMEUP+20)
{
	wifi_test();


}
finish_test_check(NULL);

reset_timer();
*/
//wifi test ends here

}

#ifdef MANUAL_TEST

void button_switch()
{
	testcase=5;
	button_detect_test_screen();
	printf("Press the POWER and MENU Buttons to observe if the pressed message comes for the corresponding buttons\n");
	start_timer_1sec();

	while(timer_testcase!=WIFI_TIMEUP)
	{

	}
	finish_test_check("NULL");

	reset_timer();

}

void longpress_switch()
{
	testcase=6;
	longpress_detect_test_screen();
	printf("Keeping the MENU button pressed, press the POWER button to observe if the POD tries to reset itself\n");
	pod_cmd_start_longpress_detect(1);		//1 is temporarily passed for testing --
	start_timer_1sec();
	while(timer_testcase!=WIFI_TIMEUP)
	{

	}
	finish_test_check("NULL");

	reset_timer();


}

void tap_switch()
{
	testcase=3;
	tap_sensor_test_screen();
	printf("\n Tap on the POD[Gap between 2 Taps - minimum 4 seconds] \n");
	start_timer_1sec();

	while(timer_testcase!=WIFI_TIMEUP)
	{
		tap_sensor_test();

	}
finish_test_check("NULL");
reset_timer();
}

void led_switch()
{
	testcase=4;

	led_detect_test_screen();
		printf("Observe if the RED and BLUE LEDs blink\n");
	led_detect:
		pod_led_detect(1);		//1 is temporarily passed for testing --
		start_timer_1sec();
		LOG_excelfmt_start("LED Blinking Done",1);
		LOG_excelfmt("PASS",LED_DETECT);

		while(timer_testcase!=5)
		{

		}
		finish_test_check("NULL");
		reset_timer();
}
#endif

extern char datefile[25];
int checkfile_empty()
{
	//printf("Time:-%s\n",current_time());
	currentdate();
	FILE *fptr;
	if ( !( fptr = fopen( datefile, "r" ))){
	    //printf( "File could not be opened to retrieve your data from it.\n" );
	    return 2;
	}
	else {
	    fseek(fptr, 0, SEEK_END);
	    unsigned long len = (unsigned long)ftell(fptr);
	    if (len > 0) {  //check if the file is empty or not.
	        rewind(fptr);
	        if ( !feof( fptr ) ){

//	            printf("not empty");
	         return 0;
	        }
	    }

	    else if(len==0){
//	    	printf("Empty\n");
	    	return 1;
	    }
	    fclose( fptr );
	}

}


#endif




#ifdef TAPDEBUG_MAIN
#include <time.h>
extern char date[10];
/*
int main()
{

	stat=pos_init();			//get POS interface
	#ifdef REPORT_STAT
	REPORT(stat);
	#endif

	stat=cmd_init();			//get CMD interface
	#ifdef REPORT_STAT
	REPORT(stat);
	#endif


//	LOG("Testbench Start",1);

	pod_pos_connect();		//Connect the POD interface of USB CDC


	while(1)
	{


	}

}*/

#endif
