/*
 * serialmanager.h
 *
 *  Created on: Oct 4, 2018
 *      Author: aksonlyaks
 */

#ifndef JNI_WIN_SERIAL_SERIALMANAGER_H_
#define JNI_WIN_SERIAL_SERIALMANAGER_H_

typedef void (*on_connect_callback)(int);
typedef void (*on_disconnect_callback)(int);
typedef void (*on_recevie_callback)(char*, int,int);
typedef void (*on_write_callback)(int);
typedef void (*on_event_received_callback)(int);

typedef struct {
	on_connect_callback on_connect_cb;
	on_disconnect_callback on_disconnect_cb;
	on_recevie_callback on_receive_cb;
	on_write_callback on_write_cb;
	on_event_received_callback on_event_rx_cb;
}serial_callback_t;

typedef struct {
	int (*init)(serial_callback_t* sintf);
	void (*deinit)(int);
	int (*connect)(char *);
	int (*disconnect)(int);
	int (*put_c)(char,int );
}serial_interface_t;
#ifdef _cplusplus
extern "C" {
#endif
//serial_interface_t* get_serial_handler(void);
#ifdef _cplusplus
}
#endif

#endif /* JNI_WIN_SERIAL_SERIALMANAGER_H_ */
