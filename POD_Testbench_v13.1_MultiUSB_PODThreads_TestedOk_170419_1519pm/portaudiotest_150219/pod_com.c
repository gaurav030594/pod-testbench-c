/*
 * pod_com.c
 *
 *  Created on: Oct 4, 2018
 *      Author: abhishek.singh
 */
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <ctype.h>

#include "serialmanager.h"
#include "log.h"
#include "pod_com.h"
#include "buffer_handle.h"
#include "com_proto.h"
#include "logger.h"
#include "debugflags.h"
#include "testcases.h"

#define TAG 	"POD_COM"


#define START_BYTE			0xA5
#define END_BYTE			0x5A
#define true				1
#define false				0
#define ENABLE				true
#define DISABLE				false

enum{
	PCKT_START_BYTE = 0,
	PCKT_CONFIG_DETAIL,
	PCKT_DATA,
	PCKT_CRC,
	PCKT_EXEC
};

enum{
	UNINITIALISED = 0,
	INITIALISED,
	CONNECTING,
	CONNECTED,
	WAITING_FOR_RESP,
	NOT_RESPONDING,
};

serial_interface_t *serial_if;	//POD Serial interface
pos_interface_t pos_intf;		//POD POS interface
cmd_interface_t cmd_intf;		//POD CMD interface


serial_callback_t serial_cb;
pos_callback_t *pos_cb;


int deinit_flag = 0;
pthread_t rx_thread[20];
int indicate_rxthread_exit[20]={ 0 };

int pos_type_var = -1;

int pod_status = POS_DISCONNECT_POD;
int pod_init_state = UNINITIALISED;

struct pos_struct{
	uint8_t status;
	uint8_t amount[AMNT_LENGTH];
	uint8_t ref[REF_LENGTH];
};
struct fwver_struct{
	uint8_t fwversion[14];

};
struct podserial_struct{
	uint8_t podserial[14];
};

void* pod_parse_thread(void* arg);
static int pos_connect(int pos_type,int devh_index);
static int pos_disconnect(int);
void parse_packet(config_setting_t *config, int devh_index);
void read_ack(config_setting_t *config, int devh_index);
void send_ack(ack_item_t ack, int pos_func_no, int devh_index);
extern serial_interface_t* get_serial_handler(void);;

int serial_data_available(int devh_index)
{

	return bufferDataAvail(&usb_buffer[devh_index]);
}

char serial_get_c(int devh_index)
{

	return bufferGetFromFront(&usb_buffer[devh_index]);
}

void on_serial_connect_cb(int devh_index)
{
#ifdef DEV_DEBUG_L2
	printf("on_ser_connect_cb,devh_index:%d\n",devh_index);
#endif
    int iret;
#ifdef DEBUG_SERIAL
    _log_error(TAG, "serial connect cb:%p\n", pos_cb);
#endif
	pos_cb->pos_pod_status_cb(POD_CONNECTING);
	indicate_rxthread_exit[devh_index] = 0;

	int *arg = malloc(sizeof(*arg));
	if ( arg == NULL ) {
		fprintf(stderr, "Couldn't allocate memory for thread arg.\n");
		exit(EXIT_FAILURE);
	}

	*arg = devh_index;
	printf("on_ser_conn_cb creating threads with devh_index:%d\n",(*(int*)arg));
    iret = pthread_create( &rx_thread[devh_index], NULL, pod_parse_thread, arg);
    if(iret != 0)
    {
#ifdef DEBUG_SERIAL
    	_log_error(TAG, "Parse thread start error:%d\n", iret);
#endif
    	return;
    }

	pod_status = POD_CONNECTING;
	pod_init_state = INITIALISED;

	pos_connect(pos_type_var,devh_index);
}

void on_serial_disconnect_cb(int devh_index)
{
	pos_cb->pos_pod_status_cb(POD_NOT_CONNECTED);
	pod_status = POD_NOT_CONNECTED;

	indicate_rxthread_exit[devh_index] = 1;

	pthread_join(rx_thread[devh_index], NULL);
}

void on_serial_receive_cb(char* buf, int size,int devh_index)
{
	//printf("on serial receive callback\n");
//#ifdef DEBUG_USB
//	printf("on serial receive callback, devh_index:%d\n",devh_index);
//#endif

	int i;
//	printf("buffer[%d] address:%p",devh_index,&usb_buffer[devh_index]);
    for(i = 0; i < size; i++)
    {

        bufferAddToEnd(&usb_buffer[devh_index], buf[i]);
    }
}

void on_serial_write_cb(int len)
{

}

void on_serial_event_cb(int event)
{
#ifdef DEBUG
	_log_error(TAG, "on_serial_event_cb\n");
#endif
	if(event ==1)
		pos_cb->pos_pod_status_cb(POD_ATTACHED);
	else
		pos_cb->pos_pod_status_cb(POD_DETACHED);
}

uint16_t CRC16native_c(char data[],int length, int start_pos) {
    char bt;
    int32_t wCrc = 0x6363;
    int idx = start_pos;
    int len= idx + length;
    do {
        bt = data[idx];
        idx++;

        bt = (char)(((bt & (0xFF)) ^ (wCrc & 0xFF)) & (0xFF));
        bt = (char)(((bt & (0xFF)) ^ ((bt << 4) & (0xFF))) & (0xFF));
        wCrc = (((wCrc) >> 8) & (0x00FFFFFF)) ^ ((bt & (0xFF)) << 8) ^ ((bt & (0xFF)) << 3) ^
        (((bt & (0xFF)) >> 4) & (0x0FFFFFFF));
    } while (idx < len);

    return wCrc;
}


#include <semaphore.h>
sem_t parse_lock;
#define CRITICAL_SECTION_START_PARSE              sem_wait(&parse_lock)
#define CRITICAL_SECTION_END_PARSE                sem_post(&parse_lock)

int sem=0;		//semaphore variable to hold and release a resource
int prev_index=0;		//for testing
void* pod_parse_thread(void* arg)
{
	int devh_index = *(int*)arg;

	free(arg);
#ifdef DEV_DEBUG_L1
	printf("pod parse thread started with devh_index:%d\n",devh_index);
#endif
	unsigned char packet_state = PCKT_START_BYTE;
	config_setting_t config;
	unsigned char *ptr, i = 0;
	uint16_t  crc_calc, *crc_ptr;
	unsigned char crc_rx[2] = {0,};
#ifdef DEBUG
	_log_error(TAG, "Parse thread started\n");
#endif


	while(indicate_rxthread_exit[devh_index] == 0 )
	{



		if(serial_data_available(devh_index))
		{
//			printf("serial_data_available,packet_state:%d ",packet_state);

			switch(packet_state)
			{
			case PCKT_START_BYTE:
//				printf("start packet of:%d ",devh_index);
				if((unsigned char)serial_get_c(devh_index) == 0xA5)
				{

					packet_state = PCKT_CONFIG_DETAIL;
					i = 0;
					ptr = (unsigned char *)&config;
				}
				break;
			case PCKT_CONFIG_DETAIL:
				*ptr = serial_get_c(devh_index);
				ptr++;
				i++;
				if(i >= 4)
				{
					packet_state = PCKT_DATA;
					i = 0;
					if(config.length > 256)
					{
						packet_state = PCKT_START_BYTE;
						_log_error(TAG, "Wrong Length %d\n", config.length);
					}
				}
				break;
			case PCKT_DATA:
				if(i < config.length)
				{
					config.arg[i] = serial_get_c(devh_index);
					i++;
				}
				else
				{
					packet_state = PCKT_CRC;
					crc_rx[0] = 0;
					crc_rx[1] = 0;
					i = 0;
				}
				break;
			case PCKT_CRC:
				if(config.length != 0)
				{
					crc_rx[i] = (unsigned char)serial_get_c(devh_index);
					i++;
				}
				else
				{
					packet_state = PCKT_EXEC;
				}
				if(i == 2)
				{
					crc_ptr = (unsigned short *)&crc_rx[0];
					crc_calc = CRC16native_c((char *)config.arg, config.length, 0);
#ifdef DEBUG_USB
					_log_error(TAG,"\n RX CRC:%x CALC CRC:%x\n", (unsigned short)*crc_ptr, crc_calc);
#endif

					if(*crc_ptr != crc_calc)
					{
						_log_error(TAG,"CRC match fail\n");
						packet_state = PCKT_START_BYTE;
						i = 0;
					}
					else
					{
						packet_state = PCKT_EXEC;
					}
				}
				break;
			case PCKT_EXEC:
				parse_packet(&config, devh_index);
				packet_state = 0;
				i = 0;
				break;
			default:
				break;
			}



		}



	}

	_log_error(TAG, "Parse thread end\n");
	return 0;
}

void send_data(config_setting_t *data_tx,int devh_index)
{
	//printf("devh_index in send_data:%d\n",devh_index);
	int tx_len, i;
	char t_data;
	char *s_data = (char *)data_tx;
	uint16_t crc = 0;

	tx_len = data_tx->length + 4;							// length of argument + 3 bytes of config_setting

	if(data_tx->length != 0)
		crc = CRC16native_c((char *)&data_tx->arg, data_tx->length, 0);
#ifdef DEBUG
	_log_error(TAG,"CRC:%x, len:%d\n", crc, data_tx->length);
	_log_error(TAG, "######## SEND ##########\n");
	_log_error(TAG, "DataType:%d SubDataTyep:%d\n", s_data[0], s_data[1]);
#endif
	// Send start byte
	t_data = START_BYTE;

	serial_if->put_c(t_data,devh_index);

#ifdef DEBUG
	_log_error(TAG, "SENT START BYTE\n");
#endif
	for(i = 0; i < tx_len; i++){
		serial_if->put_c(s_data[i],devh_index);
#ifdef DEBUG_DEV_L1
		_log_error(TAG,"%x ", s_data[i]);
#endif
	}
	if(data_tx->length != 0)
	{
		serial_if->put_c((char)crc & 0xFF,devh_index);
		serial_if->put_c((char)(crc >> 8) & 0xFF,devh_index);
	}
#ifdef DEBUG
	_log_error(TAG, "SEND END BYTE\n");
#endif
	//Send End byte
	t_data = END_BYTE;
	serial_if->put_c(t_data,devh_index);
#ifdef DEBUG
	_log_error(TAG, "######################\n");
#endif

#ifdef DEBUG
	_log_error(TAG, "SENT\n");
#endif

}



uint8_t last_amount[AMNT_LENGTH];
uint8_t fwver_string[14],podser_string[14];

extern log_csv_pod pod_log[20];
extern int pod_index;

void parse_packet(config_setting_t *config, int devh_index)			//Parsing the USB CDC received packet
{
#ifdef DEV_DEBUG_L2
	printf("parse packet:%d\n",devh_index);
#endif
	struct pos_struct *pos_rx = NULL;
	struct fwver_struct *fwver_rx = NULL;
	struct podserial_struct *podser_rx = NULL;
	struct config_struct *config_rx = NULL;
	switch(config->data_type)
	{
		case ACK_T:
			read_ack(config,devh_index);
		break;

		case CMD_T:
			if(config->sub_data_type == CMD_GET_FW_VERSION)
			{
				send_ack(ACK_OK, CMD_GET_FW_VERSION, devh_index);
				fwver_rx = (struct fwver_struct *)config->arg;
				strcpy(fwver_string,fwver_rx->fwversion);
				_log_error(TAG, "Firmware version:%s\n", fwver_string);

			}
			if(config->sub_data_type == CMD_SEND_PODSERIAL)
			{
				send_ack(ACK_OK, CMD_SEND_PODSERIAL,devh_index);
				podser_rx = (struct podserial_struct *)config->arg;


				int i = 0, c = 0; /*I'm assuming you're not using C99+*/
				for(; i < strlen(podser_rx->podserial); i++)
				{
				    if (isalnum(podser_rx->podserial[i]))
				    {
				        podser_string[c] = podser_rx->podserial[i];
				        c++;
				    }
				}
				podser_string[c] = '\0';
				_log_error(TAG, "POD Serial:%s\n", podser_string);
				strcpy(pod_log[pod_index].pod_serial,podser_string);

			}
		break;

		case POS_T:
			if(config->sub_data_type == POS_AMOUNT)
			{
				//printf("POS Amount from: %d\n",devh_index);
				send_ack(ACK_OK, POS_AMOUNT,devh_index);
				pos_rx = (struct pos_struct *)config->arg;
				if(pos_rx->status == ACK_OK){
					strcpy(last_amount,pos_rx->amount);
					_log_error(TAG, "Amount received from %d:%s\n",devh_index, pos_rx->amount);
#ifdef DEBUG
					_log_error(TAG, "Data type:%x\n", pos_rx->ref[0]);
#endif
					pos_cb->pos_info_recv_cb((unsigned char)pos_rx->ref[0], (char *)pos_rx->amount);
					memset(pos_rx->amount,0,sizeof(pos_rx->amount));
				}
				else if(pos_rx->status == ACK_TIMED_OUT)
				{
					_log_error(TAG, "Timed out\n");
					pos_cb->pos_info_recv_cb(0, NULL);
				}
			}

		break;
	}
}

#ifdef TAP_SENSOR_TEST
extern int tap_sensor_var;		//variable for tap sensor status when POD is tapped
#endif


extern int testcase;
int black_btn=0,red_btn=0;		//for black button pressed count
char menubtn_test_result=0,powerbtn_test_result=0;		//flags to store the result of menu and power btn test -- if both flags equals 1 -->test success
char longpressmenu_test_result=0;		//flags to store the result of longpress menu btn test -- if set,then longpress menu btn test success
extern char FSM_STATE_WIFI;		//extern from testcases.c

void read_ack(config_setting_t *config, int devh_index)				//Read the acknowledgement
{
#ifdef DEBUG_ACK
	_log_error(TAG, "******* ACK ********\n");
#endif
	if(config->data_type == ACK_T)
	{
		switch(config->sub_data_type)
		{
			case ACK_OK:
#ifdef DEBUG_ACK
				_log_error(TAG,"Message received successfully\n");
#endif

			break;

			case ACK_TAPPED:
#ifdef DEBUG_ACK
				_log_error(TAG, "Tap took place\n");
#endif
#ifdef TAP_SENSOR_TEST
				tap_sensor_var=1;
#endif

			break;

			case ACK_FAIL:
#ifdef DEBUG_ACK
				_log_error(TAG,"Message send fail\n");
#endif
			break;

			case ACK_RETRY:
#ifdef DEBUG_ACK
				_log_error(TAG,"Retry\n");
#endif
			break;

			case ACK_TIMED_OUT:
#ifdef DEBUG_ACK
				_log_error(TAG,"Message send timed out\n");
#endif
			break;

			case ACK_ARG_LEN:
#ifdef DEBUG_ACK
				_log_error(TAG,"Argument length wrong\n");
#endif
			break;

			case ACK_CONFIG_LEN:
#ifdef DEBUG_ACK
				_log_error(TAG,"COnfiguration length wrong\n");
#endif
			break;

			default:
#ifdef DEBUG_ACK
                _log_error(TAG,"Ack failed\n");
#endif
                config->sub_data_type = ACK_FAIL;
                break;
		}
#ifdef DEBUG_ACK
		_log_error(TAG, "ack arg:%d\n", config->arg[0]);
#endif
		if((config->arg[0] == POS_CONNECT) ||
				(config->arg[0] == POS_ALIVE))
		{
			if(config->sub_data_type >= ACK_FAIL)
			{
				pod_status = POD_NOT_CONNECTED;
				pos_cb->pos_pod_status_cb(POD_NOT_CONNECTED);
			}
			else if(config->sub_data_type == ACK_OK)
			{
				pod_status = POD_CONNECTED;
				pos_cb->pos_pod_status_cb(POD_CONNECTED);
			}
		}
		else if(config->arg[0] == POS_DISCONNECT)
		{
			pod_status = POD_NOT_CONNECTED;
			pos_cb->pos_pod_status_cb(POD_NOT_CONNECTED);
		}
		else if(config->arg[0] == ACK_TAPPED)
		{
			pos_cb->pos_tap_status_cb(1);
		}

		if(testcase == BUTTON_DETECT){

				 if(config->arg[0] == ACK_BLACK_BTN_PRESS)		//16 - Button detect -- to be sorted out in enumeration
				{
					black_btn++;
					printf("MENU button pressed:%d\n",black_btn);
					LOG_excelfmt_start("MENU Button Tested OK",1);
					menubtn_test_result=1;

				}
				 if(config->arg[0] == ACK_RED_BTN_PRESS)		//16 - Button detect -- to be sorted out in enumeration
						{
							red_btn++;
							printf("POWER button pressed:%d\n",red_btn);
							LOG_excelfmt_start("POWER Button Tested OK",1);
							powerbtn_test_result=1;

						}
		}

		else if(config->arg[0] == ACK_BLACK_LONG_PRESS)
				{

					printf("MENU Button Long pressed, Resetting POD\n");
					LOG_excelfmt_start("LONG PRESS MENU Button Tested OK",1);
					longpressmenu_test_result=1;

				}
		if(testcase == WIFI_TEST ){

		 if(config->arg[0] == ACK_WIFI_AP_CONNECTED)
				{
			 	 	FSM_STATE_WIFI=WIFI_AP_CONNECTED;


				}
		 if(config->arg[0] == ACK_WIFI_AP_NOT_CONNECTED)
				{

			 	 	FSM_STATE_WIFI=WIFI_AP_NOT_CONNECTED;

				}

		 if(config->arg[0] == ACK_WIFI_API_CONNECTED)
				{

			 	 	FSM_STATE_WIFI=WIFI_API_CONNECTED;

				}
		 printf("WIFI Reply from:%d,Wifi State:%d\n",devh_index,(int)FSM_STATE_WIFI);

//		 wifi_test(devh_index);
		}
		else
		{
			if(config->sub_data_type >= ACK_FAIL)
			{
				pos_cb->pos_api_status_cb((int)config->arg[0], 0);
			}
			else if(config->sub_data_type == ACK_OK)
			{
				pos_cb->pos_api_status_cb((int)config->arg[0], 1);
			}
		}
	}
#ifdef DEBUG_ACK
	_log_error(TAG, "****************\n");
#endif
	return;
}

void send_ack(ack_item_t ack, int pos_func_no, int devh_index)
{
#ifdef DEBUG_ACK
	printf("send ack\n");
#endif
	config_setting_t ack_var;

	ack_var.data_type = ACK_T;
	ack_var.sub_data_type = ack;
	ack_var.length = 1;
	ack_var.arg[0] = pos_func_no;

	send_data(&ack_var,devh_index);
}


/******************Serial interface functions of ToneTag POD -- entry*******************/

static int init(pos_callback_t *pos, int pos_type)
{
#ifdef DEV_DEBUG_L2
	printf("in pos init\n");
#endif
	int ret = -1;

	if(pod_init_state != UNINITIALISED)
		return 0;

	if(pos != NULL){
		pos_cb = pos;
	}
	else
		return -1;
#ifdef DEBUG
	_log_error(TAG, "Init called:%p\n", pos_cb);
#endif
	serial_if = get_serial_handler();
	if(serial_if == NULL)
	{
#ifdef DEBUG
		_log_error(TAG,"Serial Interface NULL\n");
#endif
		return -1;
	}
//    // Initialise buffer handlings
//    bufferInit(&usb_buffer,mBuffer,MaxRx);
    pos_type_var = pos_type;

	serial_if->init(&serial_cb);
	pod_init_state = INITIALISED;
#ifdef DEBUG_SERIAL
	_log_error(TAG, "Serial connect:%d\n", ret);
#endif
	return 0;
}


static void deinit(int devh_index)
{
#ifdef DEV_DEBUG_L1
	printf("in pos deinit\n");
#endif
	if(pod_init_state != INITIALISED)
		return;

	if(pod_status == CONNECTED)
		pos_disconnect(devh_index);
	if(serial_if != NULL)
	{
		serial_if->deinit(devh_index);
	}
	pod_init_state = UNINITIALISED;
}

/******************Serial interface functions of ToneTag POD -- exit*******************/



/*****************POS interface functions for initializing and performing basic POS tasks -- entry*******************/

static int pos_connect(int pos_type,int devh_index)			//Sending the CONNECT packet to the POD
{
	config_setting_t config = {0,};

	if(pod_status != POD_CONNECTING)
		return -1;
#ifdef DEBUG_USB
	_log_error(TAG, "Sending Connect packet to POD\n");
#endif
	config.data_type = POS_T;
	config.sub_data_type = POS_CONNECT;

	memcpy(config.arg, &pos_type, 2);
	send_data(&config,devh_index);
	pod_status=POD_CONNECTED;
	return 0;
}

static int pos_disconnect(int devh_index)					//Sending the DISCONNECT packet to the POD
{
	config_setting_t config;

	if(pod_status != POD_CONNECTED)
		return -1;

	_log_error(TAG, "Disconnect\n");
	config.data_type = POS_T;
	config.sub_data_type = POS_DISCONNECT;
	config.length = 0;
	send_data(&config,devh_index);
	pod_status=POD_NOT_CONNECTED;
	return 0;
}

static int pos_data_send(char *buffer, int len, int devh_index)		//Sending POS data to the POD
{
	config_setting_t config;

	if(pod_status != POD_CONNECTED)
	{
		_log_error(TAG, "POD not connected\n");
		return -1;
	}
	config.data_type = POS_T;
	config.sub_data_type = POS_DATA;
	config.length = len;
	memcpy(config.arg, buffer, len);

	send_data(&config,devh_index);
	return 0;
}

static int pos_cancel_txn(int devh_index)						//Sending Cancel TXN packet to the POD
{

	config_setting_t config;

	if(pod_status != POD_CONNECTED)
		return -1;

	_log_error(TAG, "Cancel Txn\n");
	config.data_type = POS_T;
	config.sub_data_type = POS_CANCEL_TXN;
	config.length = 0;

	send_data(&config,devh_index);
	return 0;
}

static int pos_pod_status(int devh_index)
{
	config_setting_t config;

	if(pod_status != POD_CONNECTED)
		return -1;

	_log_error(TAG, "POD Status\n");
	config.data_type = POS_T;
	config.sub_data_type = POS_ALIVE;
	config.length = 0;

	send_data(&config,devh_index);
	return 0;
}

static int pos_send_status(char *buffer, int len, int status, int devh_index)			//Send the POS status to the POD
{
	struct pos_struct pos_val;
	config_setting_t config;

	if(pod_status != POD_CONNECTED)
		return -1;

	config.data_type = POS_T;
	config.sub_data_type = POS_STATUS;
	config.length = sizeof(struct pos_struct);

    memcpy(&pos_val.amount, buffer, len);
    pos_val.status = status;

    memcpy(&config.arg, &pos_val, sizeof(struct pos_struct));

	send_data(&config,devh_index);
	return 0;
}


static int system_reset_to_boot(int devh_index)
{
	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Reset to Boot\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_RESET_TO_BOOT;
	config.length = 0;

	send_data(&config,devh_index);

	return 0;
}

/*****************POS interface functions for initializing and performing basic POS tasks -- exit*******************/


/********************CMD interface functions for sending different commands to the POD -- entry********************/


static int system_reset(int devh_index)
{
	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Reset\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_RESET;
	config.length = 0;

	send_data(&config,devh_index);

	return 0;
}


static int cmd_toneplay(int devh_index)
{


	config_setting_t config;

		if(pod_init_state != INITIALISED)
			return -1;

		_log_error(TAG, "Toneplay\n");
		config.data_type = CMD_T;
		config.sub_data_type = CMD_TONE_PLAY;
		config.length = 0;

		send_data(&config,devh_index);


		return 0;
}

void toneplay_debug(int devh_index)
{

	cmd_toneplay(devh_index);
}

static int cmd_startMic(int devh_index)
{
	config_setting_t config;

			if(pod_init_state != INITIALISED)
				return -1;

			_log_error(TAG, "Starting POD MIC\n");
			config.data_type = CMD_T;
			config.sub_data_type = CMD_START_MIC;
			config.length = 0;

			send_data(&config,devh_index);


			return 0;

}
void startMic_debug(int devh_index)
{
	 cmd_startMic(devh_index);
}

static int cmd_send_amount(int devh_index)
{
	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Requesting POD for transaction amount,pod_handle:%d\n",devh_index);
	config.data_type = CMD_T;
	config.sub_data_type = CMD_SEND_AMOUNT;
	config.length = 0;

	send_data(&config,devh_index);


	return 0;

}

static int cmd_send_TID(char* stringTID,int devh_index)
{

	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Sending the TID to POD for Transaction\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_SEND_TID;
	config.length = 10;
	memcpy(config.arg,stringTID,10);
	send_data(&config,devh_index);


	return 0;


}

static int cmd_send_podserial(int devh_index)
{
	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Fetching the Serial number of the ToneTag POD\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_SEND_PODSERIAL;
	config.length = 0;
	send_data(&config,devh_index);


	return 0;

}

static int cmd_send_wifi_stat(int devh_index)
{
	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Starting WiFi Test\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_SEND_WIFI_STAT;
	config.length = 0;
	send_data(&config,devh_index);


	return 0;

}

static int cmd_led_detect(int devh_index)
{
	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Starting LEDs Test\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_LED_DETECT;
	config.length = 0;
	send_data(&config,devh_index);


	return 0;

}

static int cmd_get_fw_version(int devh_index)
{
	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Fetching Firmware version\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_GET_FW_VERSION;
	config.length = 0;
	send_data(&config,devh_index);


	return 0;

}

static int cmd_send_wifi_stat_stop(int devh_index)
{
	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Stopping WiFi Test\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_SEND_WIFI_STAT_STOP;
	config.length = 0;
	send_data(&config,devh_index);


	return 0;

}

static int cmd_start_longpress_detect(int devh_index)
{

	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Starting Long-Press Detect Test\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_START_LONGPRESS_DETECT;
	config.length = 0;
	send_data(&config,devh_index);


	return 0;
}

static int cmd_stop_longpress_detect(int devh_index)
{

	config_setting_t config;

	if(pod_init_state != INITIALISED)
		return -1;

	_log_error(TAG, "Stopping Long-Press Detect Test\n");
	config.data_type = CMD_T;
	config.sub_data_type = CMD_STOP_LONGPRESS_DETECT;
	config.length = 0;
	send_data(&config,devh_index);


	return 0;
}
/********************CMD interface functions for sending different commands to the POD -- exit********************/


pos_interface_t* get_pos_interface(void)
{
#ifdef DEBUG
	_log_error(TAG, "POS interface\n");
#endif
	return &pos_intf;
}

cmd_interface_t* get_cmd_interface(void)
{
#ifdef DEBUG
	_log_error(TAG, "CMD interface\n");
#endif
		return &cmd_intf;

}
serial_callback_t serial_cb = {
		on_serial_connect_cb,
		on_serial_disconnect_cb,
		on_serial_receive_cb,
		on_serial_write_cb,
		on_serial_event_cb
};

pos_interface_t pos_intf = {			//POS interface for performing various POS tasks on a ToneTag POD
		init,
		deinit,
		pos_data_send,
		pos_cancel_txn,
		pos_pod_status,
		pos_send_status,
		system_reset,
		pos_disconnect,
		system_reset_to_boot
};

cmd_interface_t cmd_intf={				//CMD interface for sending various debugging commands to a ToneTag POD

		cmd_toneplay,
		cmd_startMic,
		cmd_send_amount,
		cmd_send_TID,
		cmd_send_podserial,
		cmd_send_wifi_stat,
		cmd_led_detect,
		cmd_get_fw_version,
		cmd_send_wifi_stat_stop,
		cmd_start_longpress_detect,
		cmd_stop_longpress_detect

};

