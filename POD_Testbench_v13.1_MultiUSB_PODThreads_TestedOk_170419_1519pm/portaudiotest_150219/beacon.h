/*
 * beacon.h
 *
 *  Created on: 19-Feb-2019
 *      Author: Gaurav
 */

#ifndef BEACON_H_
#define BEACON_H_

int sendU(int, int);

#define NUM_SAMPLES 512*18+512*7*40
#define BUF_SIZE (NUM_SAMPLES) // 2 second buffer

#define inputStringLength 6
#define crcBytesBuffer 2
#define bytesCount (2+3+2) //2+2+2
#define bitquadLength ((2+3+2)*2)   // ((2+2+2)*2)
#define freqArrayLength ((2+3+2)*2)   // ((2+2+2)*2)

#endif /* BEACON_H_ */
