/*
 * debugflags.h
 *
 *  Created on: 15-Feb-2019
 *      Author: Gaurav
 */
#include "testbench_config.h"

#ifndef DEBUGFLAGS_H_
#define DEBUGFLAGS_H_

#define WAIT	Sleep(2000);		//Delay
#define WAIT__  Sleep(4000);		//Extended delay

#define DEBUG_POD

#ifdef DEBUG_POD

/************DEVELOPER-LEVEL DEBUG FLAGS**************/

#define DEV_DEBUG_L1		//level-1 debugging
//#define DEV_DEBUG_L2		//level-2 debugging
//#define DEBUG
//#define DEBUG_ACK			//Enable this flag to debug the Acknowledgment service
//#define DEBUG_SERIAL		//Enable it to debug the Serial Service
//#define DEBUG_USB			//Enable it to debug the USB interface
#endif

/******Flags for enabling main of Libusb PortAudio or Beacon[Ultrasonic WAV player]*****/


//#define BEACON_MAIN					//to execute the main of beacon.c
//#define GENRAND						//to execute the main of genRandm.c
#define TESTBENCH_MAIN					//to execute the main of testbench main.c
//#define TAPDEBUG_MAIN
//#define TESTBENCH_MAIN2						//with the cli interface

#define TESTBENCH_EN		//enable the test bench
//#define REPORT_STAT			//enabling reporting of statuses during the course of execution, the dev. can disable the reporting by commenting this
//#define DEBUG_POD			//by enabling it, the Dev. can debug through various levels of execution via print statements defined in debugflags.h
#define PORTAUD_EN			//enabling it, the Dev. can use port audio to record and play for the test use case
#define LIBUSB_EN		//enable libusb in the program

#define SENDTID			//Send the TID to the POD so that it generates the sonic tone via SDK accordingly

/************USER-LEVEL DEBUG FLAGS**************/

#define USER_DEBUG_L1
//#define USER_DEBUG_L2





#endif /* DEBUGFLAGS_H_ */
