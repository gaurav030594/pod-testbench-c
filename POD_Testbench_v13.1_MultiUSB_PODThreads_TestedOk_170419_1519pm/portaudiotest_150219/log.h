//
// Created by Ibrahim Sankadal on 06/12/17.
//

#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <android/log.h>

#ifdef __cplusplus
extern "C"{
#endif

int _log_error(const char *tag, const char *msg, ...);

#ifdef __cplusplus
}
#endif
//#define printf(...) __android_log_print(ANDROID_LOG_DEBUG, "TAG", __VA_ARGS__);


#endif //LOG_H
