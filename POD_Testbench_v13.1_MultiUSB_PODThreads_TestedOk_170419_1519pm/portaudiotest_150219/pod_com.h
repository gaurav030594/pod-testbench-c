/*
 * pod_com.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Abhishek.singh@tonetag.com
 * Description: POD interface APIs and callbacks
 */

#ifndef JNI_POD_COM_H_
#define JNI_POD_COM_H_


#include "com_proto.h"
#include "debugflags.h"
// POD Status
enum {
	POD_NOT_CONNECTED = 10,
	POD_CONNECTING,
	POD_CONNECTED,
	POD_BUSY,
	POD_ATTACHED,
	POD_DETACHED
};

enum{
	INIT = 1,
	DEINIT,
	POS_CONNECT_POD,
	POS_DISCONNECT_POD,
	POS_SEND_TXN_INFO,
	POS_CANCEL_CURRENT_TXN,
	POS_POD_STATUS,
	POS_SEND_TXN_STATUS,
	POS_RESTART
};

/*
 * 		Callbacks from POD
 */
typedef void (*pos_pod_status_callback)(int);
typedef void (*pos_api_status_callback)(int, int);
typedef void (*pos_info_recv_callback)(unsigned int, char*);
typedef void (*pos_tap_status_callback)(int);

typedef struct {
	pos_pod_status_callback pos_pod_status_cb;
	pos_api_status_callback pos_api_status_cb;
	pos_info_recv_callback pos_info_recv_cb;
	pos_tap_status_callback pos_tap_status_cb;
}pos_callback_t;



void toneplay_debug(int);
void send_data(config_setting_t*,int);
/*
 * 	APIs to POD
 * 	all the APIs return 0 on successful call and -1 if it fails
 */
typedef struct {
	/*
	 * Initialise and connect to the POD
	 */
	int (*pos_init)(pos_callback_t*, int);
	/*
	 * 	uninitialise and disconnect from POD
	 */
	void (*pos_deinit)(int);
	/*
	 *	Send transaction info to POD
	 *	data: 30 Byte data
	 *	len: Length of the data
	 */
	int (*pos_send_txn_info)(char* data, int len, int devh_index);
	/*
	 * 	Cancel transaction request
	 */
	int (*pos_cancel_txn)(int);
	/*
	 *	get POD status
	 */
	int (*pos_get_pod_status)(int);
	/*
	 * 	Send Txn status to the POD
	 * 	data: Amount
	 * 	len: Length of the data
	 * 	status: status of the transaction
	 */
	int (*pos_send_txn_status)(char *data, int len, int status, int devh_index);
	/*
	 *  Restart POD
	 */
	int (*pos_restart)(int);


	int (*pos_disconnect)(int);

	int (*system_reset_to_boot)(int);
}pos_interface_t;


typedef struct {

								//Commands

int (*cmd_toneplay)(int);		//for playing the sonic tone on the ToneTag POD

int (*cmd_startMic)(int);		//for starting microphone of ToneTag POD


int (*cmd_send_amount)(int);

int (*cmd_send_TID)(char*,int);

int (*cmd_send_podserial)(int);

int (*cmd_send_wifi_stat)(int);

int (*cmd_led_detect)(int);

int (*cmd_get_fw_version)(int);

int (*cmd_send_wifi_stat_stop)(int);

int (*cmd_start_longpress_detect)(int);

int (*cmd_stop_longpress_detect)(int);




}cmd_interface_t;


/*
 * Get the interface and then use this interface to call the APIs
 */
pos_interface_t* get_pos_interface(void);
cmd_interface_t* get_cmd_interface(void);
#endif /* JNI_POD_COM_H_ */
