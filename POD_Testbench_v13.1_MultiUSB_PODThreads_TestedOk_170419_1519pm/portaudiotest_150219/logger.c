/*
 * logger.c
 *
 *  Created on: 21-Feb-2019
 *      Author: Gaurav
 */


#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "logger.h"

void gen_timestamp(char* time_string)
{
    time_t current_time;
    char* c_time_string;

    /* Obtain current time. */
    current_time = time(NULL);

    if (current_time == ((time_t)-1))
    {
        (void) fprintf(stderr, "Failure to obtain the current time.\n");
        exit(EXIT_FAILURE);
    }

    /* Convert to local time format. */
    c_time_string = ctime(&current_time);

    if (c_time_string == NULL)
    {
        (void) fprintf(stderr, "Failure to convert the current time.\n");
        exit(EXIT_FAILURE);
    }
strcpy(time_string,c_time_string);
}



//The format of the Logs is [timestamp] : ServiceName: Message
void LOG(char* logstring,int log_report)
{
	FILE *fr;
	if(log_report==1)
	{
	fr = fopen(LOGFILE_PATH, "a+");
	}
	else if(log_report==0)
	{
		fr = fopen(REPORTFILE_PATH, "a+");
	}
	else if(log_report==2)
	{

		fr = fopen(RESULTFILE_PATH, "a+");
	}
	char timestamp[30];
	gen_timestamp(timestamp);
	strtok(timestamp, "\n");			//stripping the end line character in the timestamp string
	fprintf(fr, "[ %s ] : ", timestamp);

	fprintf(fr, "%s\r\n", logstring); /*  write to file*/
	fclose(fr);

}

char s[60],datefile[25],csvstring[5]=".csv";
char filepath[]="tonetag_csv\\";
void currentdate()
{



		time_t t = time(NULL);
		struct tm *tm = localtime(&t);

		strftime(s, sizeof(s), "%c", tm);


		int i=0;

		datefile[0]=s[0];
		datefile[1]=s[1];
		datefile[2]=s[3];
		datefile[3]=s[4];
		datefile[4]='_';
		datefile[5]=s[6];
		datefile[6]=s[7];
		datefile[7]='\0';
		strcat(datefile,csvstring);

}

#define LEN 15
char time_buf[LEN];
char* current_time()
{


time_t curtime;
struct tm *loc_time;

//Getting current time of system
curtime = time (NULL);

// Converting current time to local time
loc_time = localtime (&curtime);

strftime (time_buf, LEN, "%I:%M %p", loc_time);
return time_buf;
}

void LOG_excelfmt(char* logstring,int test_case)
{
	/*
	currentdate();
	FILE *fr;
	fr = fopen(datefile, "a+");

	if(test_case == SERIALNUMBER)
	{
		fprintf(fr, "\n");
	}

	fprintf(fr, "%s,", logstring); //  write to file

	fclose(fr);
*/
}


void LOG_podstruct(log_csv_pod* podlog, int test_case)
{
	currentdate();
	FILE *fr;
	fr = fopen(datefile, "a+");
	fprintf(fr, "\n");
	if(test_case == SERIALNUMBER)
	{
		fprintf(fr, "\n");
	}
	int number_pods=get_number_pods_connected();
//	printf("number of PODS while writing logs:%d",number_pods);
	for(int i=0; i<=number_pods; i++){

//		printf("%s,%s,%s,%s,%s,%s,%s,",podlog->pod_serial,podlog->speaker_test,podlog->wifi_test,podlog->mic_test,podlog->start_time,podlog->end_time,podlog->final_result); /*  write to file*/
//		printf("\n");
	fprintf(fr, "%s,%s,%s,%s,%s,%s,%s,",podlog->pod_serial,podlog->speaker_test,podlog->wifi_test,podlog->mic_test,podlog->start_time,podlog->end_time,podlog->final_result); /*  write to file*/
	fprintf(fr, "\n");
	podlog++;
	}
	fclose(fr);


}

void LOG_excelfmt_start(char* logstring,int testcase)
{
	FILE *fr;
	fr = fopen(RESULTFILE_PATH, "a+");
	char timestamp[30];
	if(testcase==1){
	gen_timestamp(timestamp);
	strtok(timestamp, "\n");			//stripping the end line character in the timestamp string
	fprintf(fr, "[ %s ] : ", timestamp);
	}
	else{
		fprintf(fr, "POD Serial number: ");
	}
	fprintf(fr, "%s\r\n", logstring); /*  write to file*/
	fclose(fr);
}

void DATEFILE_CURRENT()
{
	currentdate();
	FILE *fr;
	fr = fopen("etc\\datecurrent.txt", "w");
	fprintf(fr, "%s", datefile); /*  write to file*/
	fclose(fr);
}
