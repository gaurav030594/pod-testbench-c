/*
 * testcases.h
 *
 *  Created on: 19-Feb-2019
 *      Author: Gaurav
 */

#ifndef TESTCASES_H_
#define TESTCASES_H_

#include <stdlib.h>
#include <pthread.h>
#include "debugflags.h"

extern int pod_handle;
extern int pod_index;
#define FOR_EVERY_POD for(int i=1; i<=get_number_pods_connected(); i++){pod_index=i; pod_handle=get_usb_handle_by_index(i);	if(pod_handle < 0){printf("Invalid POD handler\n");return 0;}//iterate through all the connected PODS

#define END }

#define HALT_EXECUTION while(1){};

typedef enum
{
PASS_FAIL=-2,
SERIALNUMBER=0,
MICROPHONE,
SPEAKER,
TAP_SENSOR,
LED_DETECT,
BUTTON_DETECT,
LONGPRESS_DETECT,
WIFI_TEST
}test_module_t;

//states for POD wifi test-case
typedef enum
{
	RESET=-1,
	START=0,
	WIFI_AP_NOT_CONNECTED,		//POD's wifi is not connected to the Hot-spot
	WIFI_AP_CONNECTED,			//when the POD's wifi is connected to the Hot-spot
	WIFI_API_NOT_CONNECTED,		//POD's wifi is not interacting with the server
	WIFI_API_CONNECTED,			//POD's wifi is interacting with the server

}wifiState;



#ifdef MIC_TEST
int mic_test(int,int);
#endif


#ifdef SPEAKER_TEST
void speaker_test(int iterations, int devh_handle);
#endif

#ifdef TAP_SENSOR_TEST


#endif


#endif /* TESTCASES_H_ */
