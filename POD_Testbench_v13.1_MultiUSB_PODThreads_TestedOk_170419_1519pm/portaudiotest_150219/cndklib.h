

#ifndef CNDKLIB_H_
#define CNDKLIB_H_
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>

#include <fcntl.h>


//#define MAC
#define WINDOWS


#define SENDER_CUSTOMER 4
#define SENDER_RETAILER 5
#define SENDER_POS 6
#define SENDER_ALL 7
#define SENDER_EDC 8

#define STRING_RECORD_MODE 0
#define BYTES_RECORD_MODE  1
#define STATUS_RECORD_MODE  2
#define AMOUNT_RECORD_MODE  3
#define TXN_STRING_RECORD_MODE 4
#define _30_BYTES_RECORD_MODE 5


int loopbreak;
char gData[32+1];

#define SIZEQ 221000
char seperatorPlyng[200];
char seperatorPld[200];
char seperatorNotPld[200];
char recvdindex[41];
char randData[20];
int toneDetected;
int toneDecripted;

int fileWriteCondition;
//char output[]="output.txt";
char output[100+1];
char reportFile[100+1];
char fileExtnsn[4+1];
char folder[100+1];
char decoded_string[32+1];	//used in cndklib.c-- copying gdata to string

int freqsToSend[40+1];

char *playSonic30Byte(char data[],char bufferRet[],int size1);
void checkIndexMatch(char SrlIndexFrmPOD[],int IndexRecieved[]);
void startNormalRecording(int recordMode, int from, bool antiDuplicationMode,int antiDuplicationVolume,int deviceVolume);
void consumeData(short data[]);

#endif /* CNDKLIB_H_ */
