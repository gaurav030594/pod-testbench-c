/*
 * com_proto.h
 *
 *  Created on: 13-Dec-2017
 *      Author: aksonlyaks
 */

#ifndef COM_PROTO_H_
#define COM_PROTO_H_


#define START_BYTE			0xA5
#define END_BYTE			0x5A
#define true				1
#define false				0
#define ENABLE				true
#define DISABLE				false

#define	TIMEOUT			-1
#define CRC_MATCH_FAIL	-2
#define WRONG_LENGTH	-3

typedef  unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int  uint32_t;
typedef int int32_t;

typedef struct
{
	/* send one byte of data */
	int (*put_c)(char value);
	/* Get one byte */
	char (*get_c)(void);
	/* Receive buffer not empty */
	uint8_t (*is_rx_ready)(void);
	/*Flush the Read buffer*/
	void (*flush_rx)(void);
} t_serial_if;

//extern t_serial_if serial_if;

// INcrease the MaxRx in the conf_buf.h when the size of this struct increases
struct config_data_t{
	uint32_t cConfigVersion;
	uint8_t cFwVersion[24];
	uint8_t cSonicTid[16];
	uint8_t cUSonicTid[16];
	uint8_t cDevPwd[4];
	uint32_t cDevNo[4];
	uint32_t cSamplingTick;
	uint32_t cSonicTick;
	uint32_t cUltraSonicTick;
	uint32_t cSetAmnt;
	uint32_t dDaysTxnAmnt;
	uint32_t dTotalTxnAmnt;
	uint32_t dDeviceBoots;
	uint8_t valid;
	uint8_t cBurstEnableOnUSB;
	uint8_t cBurstInt;
	uint8_t cEnableTap;
	uint8_t cEnableConfirm;
	uint8_t cFixedAmntMode;
	uint8_t cEnableIVR;
	uint8_t cEnableWifi;
	uint8_t cEnableKBD;
	uint8_t cEnableMICMode;
	uint8_t chw_version;
	uint8_t cUseLater;
	uint8_t cWifiName[16];
	uint8_t cWifiPwd[16];
	uint8_t cMerchantName[12];
	uint8_t cMerchantNumber[12];
	uint32_t dPrevTxnAmnt_1;
	uint32_t dPrevTxnAmnt_2;
	uint32_t dPrevTxnAmnt_3;
};
#define ARG_LEN				sizeof(struct config_data_t)

struct config_struct{
	unsigned char data_type;
	unsigned char sub_data_type;
	unsigned short length;
	unsigned char arg[ARG_LEN];
};
typedef struct config_struct config_setting_t;
extern config_setting_t pod_config;
extern struct config_data_t *d_config;

#define AMNT_LENGTH				11
#define REF_LENGTH				4

typedef enum DATA_TYPE{
	POS_T = 0,
	CONFIG_T,
	CMD_T,
	ACK_T,
	DEBUG_T,
} data_type_t;

typedef enum CONFIG_ITEMS {
	SONIC_TID = 0,
	USONIC_TID,
	BURST_INTERVAL,
	ENABLE_BURST_ON_USBPLUG,
	TAP_ENABLE,
	CONFIRM_ENABLE,
	WIFI_ENABLE,
	FIXED_AMNT_SET,
	KEYBOARD_ENABLE,
	FEATURE_PHONE_ENABLE,
	DEVICE_PASSWORD,
	DEVICE_NUMBER,
	CONFIG_VERSION,
	FW_VERSION,
	CONFIG_READ,
	CONFIG_WRITE,
	CONFIG_END		= 240,
}config_items_t;

typedef enum CMD_ITEM {
	CMD_TONE_PLAY = 0,
	CMD_USONIC_PLAY,
	CMD_START_MIC,
	CMD_RESET,
	CMD_RESET_TO_BOOT,
	CMD_RESET_TXN,
	CMD_RESET_CONFIG,
	CMD_SYSTEM_ALIVE,
	CMD_STOP_MIC,
	CMD_TONE_DATA,
	CMD_SEND_AMOUNT,
	CMD_SEND_TID,
	CMD_TAP_DETECT,
	CMD_SEND_PODSERIAL,
	CMD_SEND_WIFI_STAT,
	CMD_LED_DETECT,
	CMD_GET_FW_VERSION,
	CMD_SEND_WIFI_STAT_STOP,
	CMD_START_LONGPRESS_DETECT,
	CMD_STOP_LONGPRESS_DETECT

}cmd_item_t;

typedef enum DATA_ITEM {
	POS_CONNECT = 50,
	POS_DATA,
	POS_AMOUNT,
	POS_DISCONNECT,
	POS_CANCEL_TXN,
	POS_ALIVE,
	POS_STATUS
}data_item_t;

typedef enum ACK_ITEM {
	ACK_OK = 0,
	ACK_TAPPED,
	ACK_FAIL = 245,
	ACK_TIMED_OUT,
	ACK_RETRY,
	ACK_ARG_LEN,
	ACK_CONFIG_LEN,
	ACK_CRC_FAIL,
	ACK_BLACK_BTN_PRESS=16,
	ACK_RED_BTN_PRESS,
	ACK_BLACK_LONG_PRESS,
	ACK_WIFI_AP_CONNECTED,
	ACK_WIFI_AP_NOT_CONNECTED,
	ACK_WIFI_API_CONNECTED,


}ack_item_t;

#endif /* COM_PROTO_H_ */
