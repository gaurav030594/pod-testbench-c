/*
 * testcases.c
 *
 *  Created on: 19-Feb-2019
 *      Author: Gaurav
 */
#include "testcases.h"
#include "debugflags.h"
#include "user_interface.h"
#include "timer.h"
#include "logger.h"


#ifdef MIC_TEST


#include <stdint.h>
pthread_t sendUthread;
extern char fail_flag;
extern uint16_t mic_hits,mic_miss;

extern log_csv_pod pod_log[20];


void sendUthreadfn(void)
{
sendU(210,40);			//play Ultrasonic tone in form of WAV file containing the transaction amount
}


int mic_test(int iterations,int devh_index)
{

	int j;

	sendUthreadfn();
	WAIT

	FOR_EVERY_POD
	mic_hits=mic_miss=0;
	//pod_cmd_sendserial(pod_handle);


	//store the results of a MIC test against its serial number in a hash table
	pod_cmd_send_amount(pod_handle);		//telling the POD to send the received amount back to the TestBench for verification

	/*	WAIT

	compare_amount_result();
	if((mic_hits+mic_miss) == mic_hits && mic_hits>0)
{
		strcpy(pod_log[pod_index].mic_test,"PASS");


}
else{
	strcpy(pod_log[pod_index].mic_test,"FAIL");
	//Test Fail

	fail_flag=1;
}

	strcpy(pod_log[pod_index].end_time,current_time());
	*/
	END
}

#endif


#ifdef SPEAKER_TEST

#include "paRecPlay.h"
#include "portaudio.h"
#include "cndklib.h"
#include "beacon.h"

extern int testcase;

static void dataFound(char data[]) // This routine will be called by the record engine when data is found.
{
    printf("Data is: %s\n",data);
}


void speaker_test(int iterations, int devh_index)
{

	record(dataFound,NULL,iterations, devh_index);		//function that enables recording the SONIC tone played by the POD Speaker
	WAIT

	printf("Exiting the Recorder\n");

}


#endif



#ifdef TAP_SENSOR_TEST

int tap_sensor_var=0;
pthread_t tap_sensor_thread;
uint16_t machine_tap_detect=0;
char tap_ch[30];
char fail_flag=0;

void tap_sensor_test(void)
{

if(tap_sensor_var==1)
{
++machine_tap_detect;
sprintf(tap_ch,"Machine detected TAP %d\n",machine_tap_detect);
printf("%s",tap_ch);
LOG(tap_ch,0);
LOG(tap_ch,1);
LOG_excelfmt_start(tap_ch,1);
tap_sensor_var=0;
}

}


void tap_sensor_create_thread()		//not using currently -- 250219 1203pm
{
	int iretU;
		iretU= pthread_create( &tap_sensor_thread, NULL, tap_sensor_test, (void*)NULL);

}

#endif

char FSM_STATE_WIFI=START,FSM_PREV_STATE_WIFI=RESET;
char wifi_ch[30];
char wifi_test_result=0;
extern int timer_testcase;
int prev_devh_index=0; 	//for testing
void wifi_test(int devh_index)
{
//	printf("WIFI Reply from:%d,Wifi State:%d\n",devh_index,(int)FSM_STATE_WIFI);
	//checking the wifi state machines set by the read_ack function of pod_com.c
if(FSM_PREV_STATE_WIFI!=FSM_STATE_WIFI){


switch(FSM_STATE_WIFI)
{
case START:
	FSM_PREV_STATE_WIFI=FSM_STATE_WIFI;
	break;
case WIFI_AP_CONNECTED:
	sprintf(wifi_ch,"POD's WiFi CONNECTED to the Hot-spot:%d\n",devh_index);
	printf("%s",wifi_ch);
	LOG(wifi_ch,0);
	LOG(wifi_ch,1);
	LOG_excelfmt_start(wifi_ch,1);
	wifi_test_result=1;		//Wifi Test Pass
	FSM_PREV_STATE_WIFI=FSM_STATE_WIFI;
	timer_testcase=30;
	break;
case WIFI_AP_NOT_CONNECTED:
	sprintf(wifi_ch,"POD's WiFi NOT CONNECTED to the Hot-spot:%d\n",devh_index);
	printf("%s",wifi_ch);
	LOG(wifi_ch,0);
	LOG(wifi_ch,1);
	LOG_excelfmt_start(wifi_ch,1);
	FSM_PREV_STATE_WIFI=FSM_STATE_WIFI;
	break;

case WIFI_API_CONNECTED:
	sprintf(wifi_ch,"POD's WiFi Connected to Server\n");
	printf("%s",wifi_ch);
	LOG(wifi_ch,0);
	LOG(wifi_ch,1);
	LOG("WIFI TESTED OK",0);
	LOG("WIFI TESTED OK",1);
	FSM_PREV_STATE_WIFI=FSM_STATE_WIFI;
	//exit(1);			//exiting the program if wifi is working fine
	break;

default:
	break;
}

FSM_STATE_WIFI=START;
FSM_PREV_STATE_WIFI=RESET;
}


}

extern int timer_testcase;

extern char menubtn_test_result,powerbtn_test_result,longpressmenu_test_result; 		//external variables found and initialized in pod_com.c
void finish_test_check(int(*a)())
{

switch(testcase)
{
case LONGPRESS_DETECT:
	pod_cmd_stop_longpress_detect();
	if(longpressmenu_test_result==1)
	{
		LOG_excelfmt("PASS",LONGPRESS_DETECT);
//		LOG_excelfmt("PASS",PASS_FAIL);

	}
	else
	{
		LOG_excelfmt("FAIL",LONGPRESS_DETECT);
		fail_flag=1;
//		LOG_excelfmt("FAIL",PASS_FAIL);

	}
	break;
case WIFI_TEST:
	//pod_cmd_send_wifi_stat_stop();
	if(wifi_test_result==0){
	LOG_excelfmt("FAIL",WIFI_TEST);
	fail_flag=1;

//	LOG_excelfmt("FAIL",PASS_FAIL);

	}
	else if(wifi_test_result==1)
	{

		LOG_excelfmt("PASS",WIFI_TEST);
//		LOG_excelfmt("PASS",PASS_FAIL);

	}
	break;

case TAP_SENSOR:
	if(machine_tap_detect>0)		//if even a single tap is detected by the Test-bench then the test is PASS
	{
		LOG_excelfmt("PASS",TAP_SENSOR);
//		LOG_excelfmt("PASS",PASS_FAIL);

	}
	else
	{
		LOG_excelfmt("FAIL",TAP_SENSOR);
		fail_flag=1;

//		LOG_excelfmt("FAIL",PASS_FAIL);

	}
	break;

case BUTTON_DETECT:
	if(menubtn_test_result==1 && powerbtn_test_result==1)
	{
		LOG_excelfmt("PASS",BUTTON_DETECT);
//		LOG_excelfmt("PASS",PASS_FAIL);

	}
	else
	{
		LOG_excelfmt("FAIL",BUTTON_DETECT);
		fail_flag=1;

//		LOG_excelfmt("FAIL",PASS_FAIL);

	}
	break;


default:
	break;
}

//	}

//	}
}
