#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
//#include <synchapi.h>	//for Sleep function


#include "wave.h"
#include "beacon.h"
#include "genRandm.h"

#include "debugflags.h"




 int freqsToSend[freqArrayLength];
 int usedFreq[freqArrayLength];
 int usedFreqIndex = 0;
int tempArray[freqArrayLength];
//int freqOffset=210;
static int freqOffset=0;
int vol=0;
char charArray[16] = {'\0','0','1','2','3','4','5','6','7','8','9','A','B','C','D',' '};

unsigned char byteArray[16] = {0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xA,0xB,0xC,0xD,0xE,0xF};
unsigned char SECRET_KEY_AES[] = {0xA4, 0x83, 0x54, 0x6F, 0x38, 0xC5, 0xA6, 0x6A, 0x50, 0x11, 0x7C, 0xF2, 0xB0, 0xDD, 0x31, 0xDC};

#define size 119784
unsigned char *comp;
unsigned char crcBytes[2];
unsigned char buffer[BUF_SIZE*2];
unsigned char buffer1[size];//119784// 107800


static void addFreq1(unsigned char *buf, int offset, int sizeSamples, int freq, int amplitudeStart, int amplitudeEnd){
	int i;
	float sineWidth = sizeSamples / (float) freq;

	for(i = 0; i < sizeSamples; i++){

		float amplitude = amplitudeStart + (amplitudeEnd - amplitudeStart) * (i / (float) (sizeSamples - 1));

		short int val = (short int)(sin(i * (2 * M_PI) / sineWidth) * amplitude);

		short int oldVal = (short int)((buffer[offset * 2 + i * 2] & 0x000000FF)|((buffer[offset * 2 + i * 2 + 1] & 0x000000FF) << 8));

			val += oldVal;

			buffer[offset * 2 + i * 2] = (unsigned char) (val & 0x00ff);
			buffer[offset * 2 + i * 2 + 1] = (unsigned char) ((val & 0xff00) >> 8);


	}
}

static void addFreq(unsigned char *buf, int offset, int sizeSamples, int freq, int amplitude){

	addFreq1(buf, offset, sizeSamples, freq, amplitude, amplitude);
}


static void getToneCommon(int freqsToSend[]) {
		int toneL = 7;

		int introLength = 16 + 2;

		int i;
		for (i = 0; i < BUF_SIZE*2; i++) {
			buffer[i] = 0;
		}
		//int vol = 32000; //32000 default

		addFreq1(buffer, 0, 512, freqOffset+15, 0, vol);
		addFreq1(buffer, 512 * (introLength - 1), 512, freqOffset, vol, 0);

		for (i = 0; i < 16; i++) {
			addFreq(buffer, 512 * i + (introLength - 16) / 2 * 512, 512, freqOffset+(15-i), vol);
		}

		int mainOffset = 512 * introLength;

		for (i = 0; i < freqArrayLength; i++) {

			int toneOffset;

			toneOffset = mainOffset + 512 * (toneL * i);

			addFreq1(buffer, toneOffset, 512, freqsToSend[i], 0, vol);

			int j;
			for (j = 0; j < toneL - 2; j++) {
				addFreq(buffer, toneOffset + 512 + j * 512, 512, freqsToSend[i], vol);
			}

			addFreq1(buffer, toneOffset + (toneL - 1) * 512, 512, freqsToSend[i], vol, 0);

		}

	}
static unsigned char * crc16(unsigned char data[], int length){

	unsigned char bt = NULL;

	int wCrc = 0x6363;

	int index = 0;

	do{
		bt = data[index];
		index++;
		bt = (unsigned char) (( (bt & (0xFF)) ^ (wCrc & 0xFF)) & (0xFF));
		bt = (unsigned char) (( (bt & (0xFF)) ^ ((bt << 4) & (0xFF))) & (0xFF));
		wCrc =  (((wCrc) >> 8)&(0x00FFFFFF)) ^ ((bt & (0xFF)) << 8) ^ ((bt & (0xFF)) << 3) ^ (((bt & (0xFF)) >> 4)&(0x0FFFFFFF));
	}while(index < length);

	crcBytes[0] = (unsigned char) (wCrc & 0xFF);
	crcBytes[1] = (unsigned char) ((wCrc >> 8) & 0xFF);

	return crcBytes;
}


static unsigned char switchLeft(unsigned char in[], unsigned char addRight, int length, int shiftSize)//add one parameter called length   //2,4
	{

						int i;
						unsigned char mask = 0xFC;
						unsigned char out = (unsigned char)(  ((in[0] & mask) >> 2)&0xFF );


						for(i=0; i < length; i++)//replace 15 with parameter length
							{
								if (i+1 < length)//replace 15 with parameter length
								{
		in[i] = (unsigned char)((in[i] << shiftSize) | (  ((in[i+1] & mask)>>shiftSize)&0xFF ));//replace 6 with 4 for digits

								}else{
									in[i] = (unsigned char)((in[i] << shiftSize) | addRight);
								}
							}

			return out;
}

static unsigned char getByte(char c, char storedCharArray[], unsigned char storedByteArray[]){

	            int index = 0;
				int j;//,k;

	            for (j = 0; j < 16; j++) {
	                    if(c == storedCharArray[j]){
	                        index = j;

	                        break;
	                    }
	            }
			;
	            return storedByteArray[index];
}

static unsigned char * compString(char s[], int actualByteSize,int compressedByteSize){

	comp = (unsigned char*)malloc(compressedByteSize);;
	int i;

		for(i=0; i<actualByteSize; i++){
			char c;
	       if(i < actualByteSize){
			    c = s[i];
		    }else{
				 c = '\0';
			 }

		  unsigned char b = getByte(c, charArray, byteArray);
		   switchLeft(comp, b, 3, 4); //2,4

	   }

	return comp;

}

static void initializeFerqArray(){
	int i;
	for(i = 0; i < 40; i++){
		freqsToSend[i] = 0;
		usedFreq[i] = 0;
		tempArray[i] = 0;
	}
}

char str[inputStringLength+1];		//string to use for Ultrasonic transaction
 int sendU(int freq, int vol1)
{
	 genRandm(&str,MICROPHONE);


	freqOffset=freq;//210 default
	vol=320*vol1; //32000 default
	int i;

	initializeFerqArray();


	unsigned char *stringBytes, *stringBytesCRC;//,*stringAndCRCBytesAES;
	unsigned char stringAndCRCBytes[bytesCount-2],bytesToSend[bytesCount];
	////////////////////////////////////////

	stringBytes = compString(str, 6, 3); //4,2  actual byte, compressed byte

#ifdef DEBUG
	printf("compString\n");
	for(i=0;i<strlen((const char*)stringBytes);i++){printf("%x,",stringBytes[i]&0xff);}
	printf("\n");
#endif
	stringBytesCRC = crc16(stringBytes, 3);//2(byte after compress)

#ifdef DEBUG
	 printf("crc16\n");
	for(i=0;i<strlen((const char*)stringBytesCRC);i++){printf("%x,",stringBytesCRC[i]&0xff);}
	printf("\n");
#endif


	for(i =0; i<3; i++)
	{
		stringAndCRCBytes[i] = stringBytes[i];

	}

#ifdef DEBUG
	printf("stringAndCRCBytes\n");
	for(i=0;i<5;i++){printf("%x,",stringAndCRCBytes[i]&0xff);}
	printf("\n");
#endif

	for (i = bytesCount - 4; i < bytesCount-2; i++) {
			stringAndCRCBytes[i] = stringBytesCRC[i - (bytesCount - 4)];
		}

#ifdef DEBUG
        printf("stringAndCRCBytes\n");
	for(i=0;i<5;i++){printf("%x,",stringAndCRCBytes[i]&0xff);}
	printf("\n");
#endif




	for(i=0; i<2; i++)
	{
	   bytesToSend[i] = 1 + 16;
	  //  bytesToSend[f] = 4 + 64;
	}




	for (i = 2; i < bytesCount; i++) {
			bytesToSend[i] = stringAndCRCBytes[i - 2];
		}




	int bitQuads[bytesCount * 2];

	for (i=0; i<bytesCount*2; i++)
	{
	            unsigned char byteToSend = bytesToSend[i/2];
	            int halfByteToSend;

				if (i%2==0)
				{
				     halfByteToSend = ((byteToSend & 0x000000F0) >> 4);
				}
				else
				{
	                halfByteToSend = (byteToSend & 0x0000000F);
	            }
	            bitQuads[i] = halfByteToSend;
	}


	                                       //Un comment this for ultrasound and comment audible part
	for (i = 0; i < bytesCount * 2; i++) {
			int bitQuad = bitQuads[i];

			freqsToSend[i] = bitQuad + freqOffset;  // int freqOffset=210
		}


#ifdef DEBUG
	printf("freqsToSend \n");
	for(i=0;i<40;i++)
	{
			printf("%d ",freqsToSend[i]&0xff);
	}
	printf("\n");
#endif

	getToneCommon(freqsToSend);


	FILE * file = wavfile_open("media\\play.wav");


	if(!file) {
		printf("couldn't open sound.wav for writing: %s",strerror(errno));
		return 1;
	}



    wavfile_write(file,buffer,size);
	wavfile_close(file);
	system("bin\\sounder.exe media\\play.wav");
	free(comp);


}

#ifdef BEACON_MAIN

int main()
{
	int frequency=210;  //210 default
	int volume=40;      //Min=0%, Max=100%;
	send(frequency,volume);
	return 0;
}
#endif

