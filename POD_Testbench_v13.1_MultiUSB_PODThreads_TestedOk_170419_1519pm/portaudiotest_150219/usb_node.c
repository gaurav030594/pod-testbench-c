/*
 * usb_node.c
 *
 *  Created on: 15-Apr-2019
 *      Author: Gaurav
 */

#include "usb_node.h"


//Operations for adding and removing in a Singly Linked-List Data structure

USB_NODE* create_usb_node(int devh_handle_index, int com_port, int node_index)
{
	USB_NODE* pod_node = (USB_NODE*)malloc(sizeof(USB_NODE));
	pod_node->devh_handle_index=devh_handle_index;
	pod_node->com_port=com_port;
	pod_node->node_index=node_index;
	pod_node->next=NULL;
	return pod_node;
}

//Adding the USB NODE to the end of the Singly Linked-List, given the reference to the Head of the Singly Linked-List
USB_NODE* add_usb_node(USB_NODE** head,int devh_handle_index, int com_port, int node_index)		// return - The pointer to Head node after inserting the USB node in the linked list
{
USB_NODE* pod = create_usb_node(devh_handle_index, com_port, node_index);
if(head==NULL)	//No nodes present in the linked list, make this node as the head
{
head=pod;
return head;
}
USB_NODE* temp=head;			//temp node to traverse
while(temp->next != NULL){		//traversing till we reach the end of the Linked-List
	temp=temp->next;
}

temp->next=pod;
return head;
}

extern int usb_found[20];

//Removing the node with a specific devh_index
USB_NODE* remove_usb_node(USB_NODE** head,int devh_handle_index)	// return - The pointer to Head node after removing the USB node in the linked list
{
printf("node with handle to remove:%d\n",devh_handle_index);

USB_NODE* temp=*head,*prev,*next_node;

if(temp == NULL)
{
printf("List is empty,there is no node to be deleted\n");
return NULL;
}

if(temp!=NULL && temp->devh_handle_index==devh_handle_index)		// If head node itself holds the key to be deleted
{
printf("remove the head node\n");

next_node=temp->next;
next_node->node_index-=1;


*head=next_node;

free(temp);

return head;



}

while(temp!=NULL && temp->devh_handle_index!=devh_handle_index)
{
prev=temp;
temp=temp->next;
}

//matched
if(temp==NULL || temp->devh_handle_index!=devh_handle_index)		//if node is not found with that usb_handle
{
printf("Node not found\n");
return -1;
}

prev->next=temp->next;
free(temp);
return head;
}

int get_node_index_by_handle_index(int usb_handle_index)
{

	USB_NODE* temp=pod_list_head;
	if(temp==NULL){
		printf("The Linked-List is empty\n");
		return -1;
	}

	while(temp->devh_handle_index != usb_handle_index && temp->next != NULL)
	{
	temp=temp->next;
	}

	//matched
	if(temp->devh_handle_index == usb_handle_index)		//if matched
	{
	return temp->node_index;
	}

//	 list finished
	return -2; 		//if finished and NODE not found


}


int get_usb_handle_by_index(unsigned int node_index)
{

USB_NODE* temp=pod_list_head;
if(temp==NULL){
	printf("The Linked-List is empty\n");
	return -1;
}
if(node_index==1)
{
return temp->devh_handle_index;
}

for(unsigned int i=1; i<node_index; i++)
{
temp=temp->next;
}
return temp->devh_handle_index;
/*
while(temp->node_index != node_index && temp->next != NULL)
{
temp=temp->next;
}

//matched or list finished
if(temp->node_index==node_index)		//if matched
{
return temp->devh_handle_index;
}
return -2; 		//if finished and NODE not found
*/
}

int print_usb_nodes(USB_NODE* node_head)	//param - node_head --> head of the Linked-list to be printed, return - number of usb nodes
{
int usb_node_count=0;
if(node_head==NULL)
{
	printf("\nThe Linked-List is empty.There are no USB Nodes connected\n");
	return usb_node_count;
}

while(node_head->next != NULL)
{
usb_node_count+=1;
printf("\nUSB Node:- Devh-Handle Index:%d COM-PORT:%d USB-NODE Index:%d\n",node_head->devh_handle_index,node_head->com_port,node_head->node_index);
node_head=node_head->next;
}
usb_node_count+=1;
printf("\nUSB Node:- Devh-Handle Index:%d COM-PORT:%d USB-NODE Index:%d\n",node_head->devh_handle_index,node_head->com_port,node_head->node_index);
return usb_node_count;


}


