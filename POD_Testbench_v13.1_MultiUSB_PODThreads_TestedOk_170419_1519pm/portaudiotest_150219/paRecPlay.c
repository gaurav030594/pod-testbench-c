/*
 * paRecPlay.c
 *
 *  Created on: 18-Apr-2018
 *      Author: Sidaray
 */

#include "paRecPlay.h"
#include "debugflags.h"
#include "genRandm.h"

extern char decoded_string[32+1];		//string containing decoded TID from SDK -- used in cndklib.c



void decodeInit(const int recordMode, const int from, const bool antiDuplicationMode, const int antiDuplicationVolume, const int deviceVolume){

	startNormalRecording(recordMode, from, antiDuplicationMode, antiDuplicationVolume, deviceVolume);

}

/* This routine will be called by the PortAudio engine when audio is needed.
** It may be called at interrupt level on some machines so don't do anything
** that could mess up the system like calling malloc() or free().
*/

static int recordCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData )
{
	//printf("recordCb \n");
    paTestData *data = (paTestData*)userData;
    const SAMPLE *rptr = (const SAMPLE*)inputBuffer;

    (void) outputBuffer; // Prevent unused variable warnings.
    (void) timeInfo;
    (void) statusFlags;
    (void) userData;

    consumeData(rptr);


    if(loopbreak){
    	loopbreak=0;
    	if(gData!=NULL){
    	data->call_Back(gData);
    	}else{
    		data->call_Back(NULL);
    	}
    	//return paComplete;
    	decodeInit(STRING_RECORD_MODE,SENDER_ALL, false, 3000, 16);
    }
    return paContinue;
}

char numstring[12];		//TID string generated for Speaker Test-case to be sent to the POD

uint16_t speaker_hits=0,speaker_miss=0;		//hits and miss variables for speaker report generation
char speak_ch[10];				//used for sprintf purpose

extern int decode_done;

int record(void dFound(char*),char *usb_devArg,int iterations, int devh_index)
{

	int terminate_count=0;
    PaStreamParameters  inputParameters;
    PaStream*           stream;
    PaError             err = paNoError;
    paTestData          data;
    data.call_Back =dFound;


    decodeInit(STRING_RECORD_MODE,SENDER_ALL, false, 3000, 16);


    err = Pa_Initialize();
    if( err != paNoError ) goto done;

    inputParameters.device = Pa_GetDefaultInputDevice(); /* default input device */
    printf("inputParameters.device: %d\n",inputParameters.device);
    if (inputParameters.device == paNoDevice) {
        fprintf(stderr,"Error: No default input device.\n");
        goto done;
    }
    inputParameters.channelCount = 1;                    /* stereo input */
    inputParameters.sampleFormat = PA_SAMPLE_TYPE;
    inputParameters.suggestedLatency = Pa_GetDeviceInfo( inputParameters.device )->defaultLowInputLatency;
    inputParameters.hostApiSpecificStreamInfo = NULL;

    /* Record some audio. -------------------------------------------- */
    err = Pa_OpenStream(
              &stream,
              &inputParameters,
              NULL,                  /* &outputParameters, */
              SAMPLE_RATE,
              FRAMES_PER_BUFFER,
              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
              recordCallback,
              &data);
    if( err != paNoError ) goto done;

    err = Pa_StartStream( stream );

    if( err != paNoError ) goto done;



    while( ( err = Pa_IsStreamActive( stream ) ) == 1 )
    {



    	//generate random TID and pass it

		genRandm(&numstring,SPEAKER);			//generate RANDOM TID data for the POD speaker to play every other iteration4

		pod_cmd_send_TID(numstring,devh_index);			//send the random generated TID to the POD

		pod_cmd_toneplay(devh_index);			//send the Toneplay command to the POD



//while(decode_done!=1);			//decode_done is set by cndklib.c when the Sonic decoding is done
		Sleep(3000);
		int strcmpres = strcmp(decoded_string,numstring);			//comparing the decoded and the string sent by the test-bench to the POD
        if(strcmpres==0)										//If matched
        {
        	++speaker_hits;						//Increment the speaker_hits variable and log it in report and dev log file
        	sprintf(speak_ch,"Speaker Hits: %d",speaker_hits);

        	printf("hit \n");

        }
        else
        {
        	++speaker_miss;
        	sprintf(speak_ch,"Speaker Miss: %d",speaker_miss);
//			LOG(speak_ch,0);
//			LOG(speak_ch,1);
        	printf("miss\n");

        }
        strcpy(decoded_string," ");

        ++terminate_count;
        if(terminate_count==iterations)
        {
		err = Pa_CloseStream( stream );
		decode_done=0;
//        Pa_Terminate();

        }
    }


    if( err < 0 ) goto done;

    err = Pa_CloseStream( stream );
    if( err != paNoError ) goto done;


done:
  //  Pa_Terminate();
    if( err != paNoError )
    {

        err = 1;          /* Always return 0 or 1, but no other return codes. */
    }
    return err;
}
////////////////////////Recorder end//////////////////////////////////////////
