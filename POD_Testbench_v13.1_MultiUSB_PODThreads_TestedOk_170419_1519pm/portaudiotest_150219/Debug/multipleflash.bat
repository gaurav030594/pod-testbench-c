goto test

:test

setlocal EnableDelayedExpansion
@ECHO OFF

:portcheck

SET podfw="./podfw.bin"
SET /A Counter=0
SET port=
FOR /F %%I IN ('C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -Command "[System.IO.Ports.SerialPort]::getportnames()"
') DO (
set array[!Counter!]=%%I
set /A Counter+=1
)

set n=%Counter%
echo %n%
REM for /L %%i in (0,1,%n-1%) do echo !array[%%i]!

SLEEP 3

FOR /L %%j IN (0,1,%n%-1) DO (
  	echo %%j
	start /b bossac.exe -e -w -v -R -b %podfw% -p !array[%%j]!
)

