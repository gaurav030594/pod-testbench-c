REM pod_flash.exe

@ECHO OFF
echo ___________________________________________________________________________________________________>> test_results.txt

set startdate=%date%
set starttime=%time%
echo Testing Started on %startdate% %starttime% >> test_results.txt



call portaudiotest_150219.exe 9 1
SLEEP 7
SLEEP 4
call portaudiotest_150219.exe 1 1


echo POD Testing Completed
SLEEP 1
set /p Build=<datecurrent.txt
aescrypt.exe -e -p tonetag %Build%
set stopdate=%date%
set stoptime=%time%
echo Testing Completed on %stopdate% %stoptime% >> test_results.txt

echo ___________________________________________________________________________________________________ >> test_results.txt
