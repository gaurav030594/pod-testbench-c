//
// Created by Abhishek Singh on 29/01/18.
//

#ifndef ANDROIDUSBSERIALCOMM_BUFFER_HANDLE_H
#define ANDROIDUSBSERIALCOMM_BUFFER_HANDLE_H
#include <stdio.h>
#include <stdint.h>

//! cBuffer structure
typedef struct struct_cBuffer
{
    uint8_t *dataptr;                 ///< the physical memory address where the buffer is stored
    uint16_t size;                    ///< the allocated size of the buffer
    uint16_t datalength;              ///< the length of the data currently in the buffer
    uint16_t dataindex;               ///< the index into the buffer where the data starts
} cBuffer;

#define MaxRx			1000

extern cBuffer usb_buffer[20];                           // Receive Buffer
extern unsigned char mBuffer[MaxRx];

// function prototypes

//! initialize a buffer to start at a given address and have given size
void bufferInit(cBuffer* buffer, unsigned char *start, unsigned short size);

//! get the first byte from the front of the buffer
char   bufferGetFromFront(cBuffer* buffer);

//! dump (discard) the first numbytes from the front of the buffer
void bufferDumpFromFront(cBuffer* buffer, unsigned short numbytes);

//! It reads from front does not modify the index
unsigned char  bufferReadFromFront(cBuffer* buffer);
//! get a byte at the specified index in the buffer (kind of like array access)
// ** note: this does not remove the byte that was read from the buffer
unsigned char   bufferGetAtIndex(cBuffer* buffer, unsigned short index);


//! add a byte to the end of the buffer
unsigned char   bufferAddToEnd(cBuffer* buffer, unsigned char data);

//! check if the buffer is full/not full (returns zero value if full)
unsigned short  bufferIsNotFull(cBuffer* buffer);

//! returns number of bytes available to read
unsigned char bufferDataAvail(cBuffer* buffer);

//! flush (clear) the contents of the buffer
void  bufferFlush(cBuffer* buffer);

#endif //ANDROIDUSBSERIALCOMM_BUFFER_HANDLE_H
