//
// Created by Abhishek Singh on 29/01/18.
//
#include <semaphore.h>
#include "buffer_handle.h"



unsigned char mBuffer[MaxRx];
cBuffer usb_buffer[20];		//20 - for the number of pods
sem_t lock;

#define CRITICAL_SECTION_START              sem_wait(&lock)
#define CRITICAL_SECTION_END                sem_post(&lock)


//#define serial_send(a)                                     	// Transmit char a
//#define serial_rx_ready() 1												// Receiver full
//#define serial_get(buff)                    bufferGetFromFront(&usb_buffer)        				// Receive char from modem


void bufferInit(cBuffer* buffer, unsigned char *start, unsigned short size)
{

    sem_init(&lock, 0, 1);
    // begin critical section
    CRITICAL_SECTION_START;
    // set start pointer of the buffer
    buffer->dataptr = start;
    buffer->size = size;
    // initialize index and length
    buffer->dataindex = 0;
    buffer->datalength = 0;
    // end critical section
    CRITICAL_SECTION_END;

}

// access routines
char  bufferGetFromFront(cBuffer* buffer)
{
    char data = 0;
    // begin critical section
    CRITICAL_SECTION_START;
    // check to see if there's data in the buffer
    if(buffer->datalength)
    {
        // get the first character from buffer
        data = buffer->dataptr[buffer->dataindex];
        // move index down and decrement length
        buffer->dataindex++;
        if(buffer->dataindex >= buffer->size)
        {
            buffer->dataindex -= buffer->size;
        }
        buffer->datalength--;
    }
    // end critical section
    CRITICAL_SECTION_END;
    // return
    return data;
}

// Reads data from buffer but does not affects the data index
unsigned char  bufferReadFromFront(cBuffer* buffer)
{
    unsigned char data = 0;
    // begin critical section
    CRITICAL_SECTION_START;
    // check to see if there's data in the buffer
    if(buffer->datalength)
    {
        // get the first character from buffer
        data = buffer->dataptr[buffer->dataindex];
        // move index down and decrement length
        buffer->dataindex++;
        if(buffer->dataindex >= buffer->size)
        {
            buffer->dataindex -= buffer->size;
        }
        //              buffer->datalength--;
    }
    // end critical section
    CRITICAL_SECTION_END;
    // return
    return data;
}

void bufferDumpFromFront(cBuffer* buffer, unsigned short numbytes)
{
    // begin critical section
    CRITICAL_SECTION_START;
    // dump numbytes from the front of the buffer
    // are we dumping less than the entire buffer?
    if(numbytes < buffer->datalength)
    {
        // move index down by numbytes and decrement length by numbytes
        buffer->dataindex += numbytes;
        if(buffer->dataindex >= buffer->size)
        {
            buffer->dataindex -= buffer->size;
        }
        buffer->datalength -= numbytes;
    }
    else
    {
        // flush the whole buffer
        buffer->datalength = 0;
    }
    // end critical section
    CRITICAL_SECTION_END;
}

unsigned char bufferGetAtIndex(cBuffer* buffer, unsigned short index)
{
    // begin critical section
    CRITICAL_SECTION_START;
    // return character at index in buffer
    unsigned char data = buffer->dataptr[(buffer->dataindex+index)%(buffer->size)];
    // end critical section
    CRITICAL_SECTION_END;
    return data;
}

unsigned char bufferAddToEnd(cBuffer* buffer, unsigned char data)
{
    // begin critical section
    CRITICAL_SECTION_START;
    // make sure the buffer has room
    if(buffer->datalength < buffer->size)
    {
//    	printf("adding to buffer");
        // save data byte at end of buffer
        buffer->dataptr[(buffer->dataindex + buffer->datalength) % buffer->size] = data;
        // increment the length
        buffer->datalength++;
        // end critical section
        CRITICAL_SECTION_END;
        // return success
        return 0;
    }

    printf("buffer add to end failure ");
    // end critical section
    CRITICAL_SECTION_END;
    // return failure
    return 1;
}

unsigned short bufferIsNotFull(cBuffer* buffer)
{
    // begin critical section
    CRITICAL_SECTION_START;
    // check to see if the buffer has room
    // return true if there is room
    unsigned short bytesleft = (buffer->size - buffer->datalength);
    // end critical section
    CRITICAL_SECTION_END;
    if(bytesleft)
        return 1;
    else
        return 0;
}

unsigned char bufferDataAvail(cBuffer* buffer)
{
    uint16_t data;
    // begin critical section
    CRITICAL_SECTION_START;
    data = buffer-> datalength;
    // end critical section
    CRITICAL_SECTION_END;
    if(data > 0)
    	return 1;
    else
    	return 0;
}

void bufferFlush(cBuffer* buffer)
{
    // begin critical section
    CRITICAL_SECTION_START;
    // flush contents of the buffer
    buffer->datalength = 0;
    buffer->dataindex = 0;
    // end critical section
    CRITICAL_SECTION_END;
}
