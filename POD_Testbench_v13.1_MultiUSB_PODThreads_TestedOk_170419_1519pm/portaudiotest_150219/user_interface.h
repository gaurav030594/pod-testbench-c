/*
 * user_interface.h
 *
 *  Created on: 22-Feb-2019
 *      Author: Gaurav
 */

#ifndef USER_INTERFACE_H_
#define USER_INTERFACE_H_

#define USER_INPUT manual_input

typedef enum
{
MANUAL=1,
AUTONOMOUS
}test_mode_t;

typedef enum
{
YES=1,
NO
}yes_no_input;

void user_input(void);
void test_mode(void);
void welcome_screen(void);

int manual_input;		//This variable will be used throughout the test-bench to take User-inputs and then act on it accordingly.

#endif /* USER_INTERFACE_H_ */
