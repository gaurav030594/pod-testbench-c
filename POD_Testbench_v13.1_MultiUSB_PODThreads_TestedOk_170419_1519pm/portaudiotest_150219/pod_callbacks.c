/*
 * pod_callbacks.c
 *
 *  Created on: 25-Feb-2019
 *      Author: Gaurav
 */

#include "debugflags.h"
#include "pod_callbacks.h"
#include "serialmanager.h"






/***************Callbacks -- entry****************************/

void pod_status_cb(int stat)
{
#ifdef DEV_DEBUG_L2
					printf("pod status cb, status:%d \n",stat);
#endif

}

void pod_api_status_cb(int API, int stat)
{
#ifdef DEV_DEBUG_L2
					printf("pod api status cb, status:%d \n",stat);
#endif

}

void pod_info_recv_cb(unsigned int stat, char* Amount)
{

#ifdef DEV_DEBUG_L2
					printf("pod info receive cb, status:%d \n",stat);
#endif
}

void pod_tap_status_cb(int stat)
{

#ifdef DEV_DEBUG_L2
					printf("pod tap status cb, status:%d \n",stat);
#endif

}

void pod_on_data_rx_cb(char* data, int status)
{

#ifdef DEV_DEBUG_L2
					printf("pod on rx cb, status:%d \n",status);
#endif

}

void pod_on_event_cb(int status)
{

#ifdef DEV_DEBUG_L2
					printf("pod on event cb, status:%d \n",status);
#endif

}

void ser_on_connect_cb()
{
#ifdef DEV_DEBUG_L2
					printf("ser on connect cb\n");
#endif
printf("callback\n");
}

void ser_on_disconnect_cb()
{
#ifdef DEV_DEBUG_L2
					printf("ser on disconnect cb\n");
#endif
}

void ser_on_receive_cb(char* rxdata, int stat)
{

#ifdef DEV_DEBUG_L2
					printf("ser on receive cb\n");
#endif
}

void ser_on_write_cb(int stat)
{

#ifdef DEV_DEBUG_L2
					printf("ser on write cb,stat:%d\n",stat);
#endif
}

void ser_on_event_rx_cb(int stat)
{

#ifdef DEV_DEBUG_L2
					printf("ser on eventrx cb,stat:%d\n",stat);
#endif
}

/***************Callbacks -- exit****************************/

/***************Callback structure -- entry***************************/

 pos_callback_t pos_status_cb = {
		pod_status_cb,
		pod_api_status_cb,
		pod_info_recv_cb,
		pod_tap_status_cb
};

static Tserial_event_callback_t podserial_event_cb = {
		pod_on_event_cb,
		pod_on_data_rx_cb

};


/***************Callback structure -- exit***************************/
