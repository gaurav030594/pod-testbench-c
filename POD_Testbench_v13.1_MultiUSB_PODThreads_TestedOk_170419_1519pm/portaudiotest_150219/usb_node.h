/*
 * usb_node.h
 *
 *  Created on: 15-Apr-2019
 *      Author: Gaurav
 */

#ifndef USB_NODE_H_
#define USB_NODE_H_

#include "Tserial_event.h"		//for using pod_list_head extern pointer to the node

#define NULL ((void *)0)



typedef struct {
	int devh_handle_index;
	int com_port;
	int node_index;
	struct USB_NODE* next;
}USB_NODE;


extern USB_NODE* pod_list_head;		// POD USB Node pointer declared and initialized in Tserial_event.c


USB_NODE* create_usb_node(int devh_handle_index, int com_port, int node_index);
USB_NODE* add_usb_node(USB_NODE** head,int devh_handle_index, int com_port, int node_index);
USB_NODE* remove_usb_node(USB_NODE** head,int devh_handle_index);
int get_usb_handle_by_index(unsigned int node_index);
int get_node_index_by_handle_index(int usb_handle_index);

int print_usb_nodes(USB_NODE* node_head);


#endif /* USB_NODE_H_ */
