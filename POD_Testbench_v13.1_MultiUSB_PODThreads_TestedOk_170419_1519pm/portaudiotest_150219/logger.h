/*
 * logger.h
 *
 *  Created on: 21-Feb-2019
 *      Author: Gaurav
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include "debugflags.h"
#include "testcases.h"

typedef struct
{
char pod_serial[20];
char speaker_test[5];
char wifi_test[5];
char mic_test[5];
char start_time[15];
char end_time[15];
char final_result[5];

}log_csv_pod;

void gen_timestamp(char*);
void LOG(char*,int);
void LOG_excelfmt(char* logstring,int test_case);
void LOG_excelfmt_start(char*,int);
void LOG_podstruct(log_csv_pod*, int);

#endif /* LOGGER_H_ */
