


/* ------------------------------------------------------------------------ --
--                                                                          --
--                  Application example for using Tserial_event             --
--                                                                          --
--                                                                          --
--                                                                          --
--  Copyright @ 2001-2002     Thierry Schneider                             --
--                            thierry@tetraedre.com                         --
--                                                                          --
--                                                                          --
--                                                                          --
-- ------------------------------------------------------------------------ --
--                                                                          --
--  Filename : serialtest.cpp                                               --
--  Author   : Thierry Schneider                                            --
--  Created  : April 8th 2001                                               --
--  Modified : January 30th 2002                                            --
--  Plateform: Windows 95, 98, NT, 2000 (Win32)                             --
-- ------------------------------------------------------------------------ --
--                                                                          --
--  This software is given without any warranty. It can be distributed      --
--  free of charge as long as this header remains, unchanged.               --
--                                                                          --
-- ------------------------------------------------------------------------ --
--                                                                          --
--  01.04.24    # ifdef __BORLANDC__ added in the header                    --
--                                                                          --
-- 02.01.30      Version 2.0 of the serial event object                     --
--                                                                          --
--                                                                          --
-- ------------------------------------------------------------------------ */


/* ---------------------------------------------------------------------- */

//---------------------------------------------------------------------------
#include <stdio.h>
#include "Tserial_event.h"
#include "serialmanager.h"
#include "log.h"
#include "debugflags.h"


#define TAG		"SERIAL"


serial_callback_t *ser_cb;
Tserial_interface_t* com;

int serial_init(serial_callback_t *sintf);
void serial_deinit(int);
int serial_connect(char *com_port);
int serial_disconnect(void);
int serial_write(char buffer,int devh_index);
void SerialEventManager(int event,int devh_index);
void OnDataArrival(char *buffer, int size, int devh_index);

serial_interface_t ser_inf = {
		serial_init,
		serial_deinit,
		serial_connect,
		serial_disconnect,
		serial_write,
};

Tserial_event_callback_t serial_event_cb = {
		SerialEventManager,
		OnDataArrival
};

/*
static void ser_on_connect_cb()
{
printf("callback\n");
}

serial_callback_t pod_serial_cb={

		 ser_on_connect_cb,
		 NULL,
		 NULL,
		 NULL,
		 NULL
};*/
//extern "C" serial_interface_t* get_serial_handler(void);

serial_interface_t* get_serial_handler(void)
{
	return &ser_inf;
}
/* ======================================================== */
/* ===============  OnCharArrival     ===================== */
/* ======================================================== */
void OnDataArrival(char *buffer, int size, int devh_index)
{

    if ((size>0) && (buffer!=0))
    {
//    	printf("data for: %d ",devh_index);
#ifdef DEBUG
    	_log_error(TAG, "size:%d\n", size);
#endif
        if(ser_cb != NULL)
        	ser_cb->on_receive_cb(buffer, size, devh_index);
    }
}


/* ======================================================== */
/* ===============  OnCharArrival     ===================== */
/* ======================================================== */
void SerialEventManager(int event,int devh_index)
{

#ifdef USER_DEBUG_L2
					_log_error(TAG, "Inside SerialEventManager,devh_index:%d\n",devh_index);
#endif
    if (com!=0)
    {
        switch(event)
        {
            case  SERIAL_CONNECTED  :
#ifdef USER_DEBUG_L2
                                        _log_error(TAG, "Serial Connected ! \n");
#endif
                                        if(ser_cb != NULL)
                                        	ser_cb->on_connect_cb(devh_index);
                                        break;
            case  SERIAL_DISCONNECTED  :
                                        _log_error(TAG, "Disconnected ! \n");
                                        if(ser_cb != NULL)
                                        	ser_cb->on_disconnect_cb(devh_index);
                                        break;
            case  SERIAL_DATA_SENT  :
                                        _log_error(TAG, "Data sent ! \n");
                                        if(ser_cb != NULL)
                                        	ser_cb->on_write_cb(0);
                                        break;
            case SERIAL_ATTACHED:
#ifdef DEBUG_SERIAL
            							_log_error(TAG, "Serial device Attached\n");
#endif
            							if(ser_cb != NULL)
            								ser_cb->on_event_rx_cb(1);
            							break;

            case SERIAL_DETACHED:
            							_log_error(TAG, "Serial device detached\n");
            	           				if(ser_cb != NULL)
            	           					ser_cb->on_event_rx_cb(0);

            							break;

            case SERIAL_INCORRECT_DRIVER:
            							_log_error(TAG, "Incorrect Driver; Install WINUSB\n");
            	           				if(ser_cb != NULL)
            	           					ser_cb->on_event_rx_cb(2);
            							break;
        }
    }
}

int serial_init(serial_callback_t *sintf)
{
	if(sintf != NULL)
		ser_cb = sintf;
	else
		return -1;

	com = get_Tserial_interface();
#ifdef DEBUG_SERIAL
	_log_error(TAG, "%s","Generic Serial interface not null");
#endif
    if (com!=0)
    {
        com->init(&serial_event_cb);
    }
    else
    {
#ifdef DEBUG_SERIAL
    	_log_error(TAG, "com is 0\n");
#endif
    	return -2;
    }
#ifdef DEBUG_SERIAL
    _log_error(TAG, "Serial Interface init\n");
#endif
	return 0;
}

void serial_deinit(int devh_index)
{
	if(com != NULL)
		com->deinit(devh_index);
}

int serial_connect(char *com_port)
{

	return 0;
}

int serial_disconnect(void)
{
	return 1;
}

int serial_write(char buffer, int devh_index)
{
//	printf("in serial write\n");
	//printf("devh_index:%d\n",devh_index);
	if(com != NULL)
		com->sendData(&buffer, 1, devh_index);

	return 0;
}

/* ======================================================== */
/* ========================   main  ======================= */
/* ======================================================== */
/*
extern "C" {
int main_new(void);
};
int main_new(void)
{
    int            c;
    int            erreur;
    char           txt[32];
    Tserial_event *com;

    com = new Tserial_event();
    if (com!=0)
    {
        com->setManager(SerialEventManager);
        erreur = com->connect("\\\\.\\COM62", 19200, SERIAL_PARITY_NONE, 8, true);
        if (!erreur)
        {
            com->sendData("Hello World",11);
            com->setRxSize(5);

            // ------------------
            do
            {
                c = 'D';//getch();
                fprintf(stdout, "_%c",c);
                fflush(stdout);
                txt[0] = c;
                com->sendData(txt, 1);
                com->setRxSize(1);
            }
            while (0);

        }
        else
            printf("ERROR : com->connect (%ld)\n",erreur);
        // ------------------
        com->disconnect();

        // ------------------
        delete com;
        com = 0;
    }
    return 0;
}
*/

