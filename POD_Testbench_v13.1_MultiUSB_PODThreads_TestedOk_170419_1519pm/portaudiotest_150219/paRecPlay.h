/*
 * paRecPlay.h
 *
 *  Created on: 18-Apr-2018
 *      Author: Sidaray
 */

#ifndef PARECPLAY_H_
#define PARECPLAY_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
//#include "podPlay.h"
#include "portaudio.h"
#include "cndklib.h"

#define STRING_RECORD_MODE 0
#define BYTES_RECORD_MODE  1
#define STATUS_RECORD_MODE  2
#define AMOUNT_RECORD_MODE  3
#define TXN_STRING_RECORD_MODE 4
#define _30_BYTES_RECORD_MODE 5

#define SENDER_CUSTOMER 4
#define SENDER_RETAILER 5
#define SENDER_POS  6
#define SENDER_ALL 7
#define SENDER_EDC 8


#define NUM_SAMPLES 512*18+512*7*40
#define BUF_SIZE (NUM_SAMPLES*2)

#define _1BFFT_BUFFER_LENGTH 128
#define _1BFREQ_REP 7
#define _1BmNumSamples  _1BFFT_BUFFER_LENGTH *_1BFREQ_REP * 8
#define _1BmGeneratedSndSize _1BmNumSamples*2
#define Dsize 221000
#define _14bBsize 158800//199784


/* #define SAMPLE_RATE  (17932) // Test failure to open with this value. */
#define SAMPLE_RATE  (44100)
#define FRAMES_PER_BUFFER (1024)
#define NUM_SECONDS     (4) //0.03
#define NUM_CHANNELS    (1)
/* #define DITHER_FLAG     (paDitherOff) */
#define DITHER_FLAG     (0) /**/
/** Set to 1 if you want to capture the recording to a file. */


/* Select sample format. */
#if 0
#define PA_SAMPLE_TYPE  paFloat32
typedef float SAMPLE;
#define SAMPLE_SILENCE  (0.0f)
#define PRINTF_S_FORMAT "%.8f"
#elif 1
#define PA_SAMPLE_TYPE  paInt16
typedef short SAMPLE;
#define SAMPLE_SILENCE  (0)
#define PRINTF_S_FORMAT "%d"
#elif 0
#define PA_SAMPLE_TYPE  paInt8
typedef char SAMPLE;
#define SAMPLE_SILENCE  (0)
#define PRINTF_S_FORMAT "%d"
#else
#define PA_SAMPLE_TYPE  paUInt8
typedef unsigned char SAMPLE;
#define SAMPLE_SILENCE  (128)
#define PRINTF_S_FORMAT "%d"
#endif

#define PA_SAMPLE_TYPE1  paInt8
typedef char SAMPLE1;
#define SAMPLE_SILENCE1  (0)
#define PRINTF_S_FORMAT1 "%d"

/*extern char output[23+1];
extern char reportFile[18+1];
extern char fileExtnsn[4+1];
extern char folder[9+1];
extern int freqsToSend[40+1];*/

typedef struct
{
    int          frameIndex;  /* Index into sample array. */
    int          maxFrameIndex;
    SAMPLE      *recordedSamples;
    void (*call_Back) (char data[]);
}
paTestData;

typedef struct
{
    int          frameIndex1;  /* Index into sample array. */
    int          maxFrameIndex1;
    SAMPLE1      *recordedSamples1;
}
paTestData1;
//typedef void (*call_Back) (char data[]);
//int record(call_Back back);
int record(void dFound(char*),char *usb_devArg,int iterations, int devh_index);
int playFrom_paAudio(char adioBuff[],int len);
#endif /* PARECPLAY_H_ */
