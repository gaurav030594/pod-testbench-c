/*
 * testbench_config.h
 *
 *  Created on: 30-Jan-2019
 *      Author: Gaurav
 */

#ifndef TESTBENCH_CONFIG_H_
#define TESTBENCH_CONFIG_H_



#define USER_INTERFACE_EN		//Enable it to use the User Interface that runs with the executable



/******Specify the path to the Log file in which you want to log information********************/
#define REGISTER_LOGS


#ifdef REGISTER_LOGS

//Absolute path of the log file to be provided that needs to be generated --- For Developer Troubleshooting
//#define LOGFILE_PATH 			"C:/Users/Gaurav/eclipse-workspace/portaudiotest_150219/Debug/dev_logs.txt"
//#define REPORTFILE_PATH			"C:/Users/Gaurav/eclipse-workspace/portaudiotest_150219/Debug/user_report.txt"
#define LOGFILE_PATH 			"logs/dev_logs.txt"
#define REPORTFILE_PATH			"logs/user_report.txt"
#define EXCELREPORT_PATH		"logs/user_report_csvfmt.csv"
#define RESULTFILE_PATH			"logs/test_results.txt"
#define PASS_FAIL_LOG_PATH		"logs/pass_fail.txt"
#define DATEFILE				"03/20/19.csv"
#endif



#define MIC_TEST
#define SPEAKER_TEST


#define SPEAKER_TEST_EN
#define WIFI_TEST_EN
#define MIC_TEST_EN
#define SEND_SERIAL_EN
//#define POD_FLASH_EN


//#define MANUAL_TEST		//uncomment it to enable manual testing such as LED,Longpress,Single-press and TAP test


//#define GENERATE_REPORT
#ifdef GENERATE_REPORT


#endif


/*****Flags for enabling/disabling the unit tests of ToneTag POD**********/



#ifdef MIC_TEST

#endif



#ifdef SPEAKER_TEST
#endif



#ifdef WIFI_TEST_EN
#endif

#define TAP_SENSOR_TEST

#ifdef TAP_SENSOR_TEST



#endif

#endif /* TESTBENCH_CONFIG_H_ */
