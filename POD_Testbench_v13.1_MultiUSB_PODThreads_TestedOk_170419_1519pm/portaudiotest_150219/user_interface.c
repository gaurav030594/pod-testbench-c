/*
 * user_interface.c
 *
 *  Created on: 22-Feb-2019
 *      Author: Gaurav
 */


#include "user_interface.h"


void user_input()
{
	printf("\nEnter your choice:-");
	scanf("%d",&manual_input);			//use sscanf for better input -- improvement to be done

}

void test_mode()
{
	//printing the test mode choices for the user
	printf("Would you prefer? \n 1. MANUAL TESTING\n 2. AUTONOMOUS TESTING\n");
	user_input();		//Taking the user input


}

void welcome_screen()
{

	printf("\n*******************ToneTag POD Unit Testing********************\n\n");
	printf("Select the component of the ToneTag POD you want to test:-\n\n");
	printf(" 1. MICROPHONE\n 2. SPEAKER\n 3. TAP SENSOR\n 4. OLED DISPLAY\n 5. BUTTON\n 6. WiFi Module\n");
	user_input();
}

void manual_test_screen()
{
	printf("Manual testing starts, Get Ready for your inputs\n");

}

void autonomous_test_screen()
{
	printf("Autonomous testing starts\n\n");
	printf("Enter the number of tests the machine shall do\n");
	user_input();		//Taking the user input -- Number of iterations for the test, the machine shall do autonomously
}

void mic_test_screen()
{
	printf("\n\n********************MICROPHONE TEST*********************\n\n");

}

void speaker_test_screen()
{
	printf("\n\n********************SPEAKER TEST*********************\n\n");

}

void tap_sensor_test_screen()
{
	printf("\n\n********************TAP SENSOR TEST*********************\n\n");

}

void led_detect_test_screen()
{
	printf("\n\n********************LED TEST*********************\n\n");
}

void button_detect_test_screen()
{
	printf("\n\n********************BUTTONS TEST*********************\n\n");
}

void longpress_detect_test_screen()
{
	printf("\n\n********************LONG-PRESS BUTTON TEST*********************\n\n");
}
void wifi_test_screen()
{
	printf("\n\n********************POD WIFI TEST*********************\n\n");
	printf("Switch On the WIFI Hotspot with SSID - Tone and PASSWD - ''\n");
/*	printf("After switching the HOT-SPOT ON, Enter 1 and press Enter key\n");
	user_input();
	if(manual_input==1)
	{
		//pass through the test
	}
	else{
		//re-run the wifi test
	}*/
}

void finish_test_screen()
{
	printf("Do you want to finish this test 1. YES  2.NO\n");
	user_input();
}
