//
// Created by Ibrahim Sankadal on 06/12/17.
//
#include <stdio.h>
#include <stdarg.h>
#include "log.h"
#include "debugflags.h"


// for console logs
int _log_error(const char *tag, const char *fmt, ...){
	//Enable DEBUG flag in debugflags.h for logs.
	int done = 0;

    va_list arg;


    va_start (arg, fmt);
    done = vfprintf (stdout, fmt, arg);
    LOG(fmt,1);
    va_end (arg);
    fflush(stdout);//uncomment for logs.

    return done;


}
