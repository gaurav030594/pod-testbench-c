/*
 * Tserial_event.h
 *
 *  Created on: Jan 18, 2019
 *      Author: aksonlyaks
 */

#ifndef JNI_GENERIC_SERIAL_TSERIAL_EVENT_H_
#define JNI_GENERIC_SERIAL_TSERIAL_EVENT_H_
#define SERIAL_PARITY_NONE 0
#define SERIAL_PARITY_ODD  1
#define SERIAL_PARITY_EVEN 2

#define SERIAL_CONNECTED         0
#define SERIAL_DISCONNECTED      1
#define SERIAL_DATA_SENT         2
#define SERIAL_DATA_ARRIVAL      3
#define SERIAL_ATTACHED			 4
#define SERIAL_DETACHED			 5
#define SERIAL_INCORRECT_DRIVER	 6

typedef void (*on_event_callback)(int,int);
typedef void (*on_read_callback)(char*, int, int);

typedef struct {
	on_event_callback on_event_cb;
	on_read_callback on_data_rx_cb;

}Tserial_event_callback_t;

typedef struct {
	int (*init)(Tserial_event_callback_t* serial_cb);
	void (*sendData)(char *buffer, int size,int devh_index);
	char* (*getDataInBuffer)(void);
	int (*getDataInSize)(void);
	void (*deinit)(int);
}Tserial_interface_t;

Tserial_interface_t* get_Tserial_interface(void);


int get_number_pods_connected(void);
int read_chars(unsigned char* , int,int);
void write_char(unsigned char,int);

#endif /* JNI_GENERIC_SERIAL_TSERIAL_EVENT_H_ */
