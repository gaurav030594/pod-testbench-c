/*
 * Tserial_event.c
 *
 *  Created on: 16-Jan-2019
 *      Author: aksonlyaks
 */


//Standard C header files

#include <stdio.h>
#include <pthread.h>

//LibUSB header file
#include "libusb.h"

//ToneTag header files

#include "Tserial_event.h"
#include "log.h"

#include "debugflags.h"		//enabling and disabling the debug flags
//#include "status_logs.h"	//for reporting the status
#include "pod_com.h"		//! added 110419

#include "usb_node.h"		//for adding and removing usb_nodes in the data structure -- to be referenced
#include "buffer_handle.h"

#define TAG "TSERIAL"
/* You may want to change the VENDOR_ID and PRODUCT_ID
 * depending on your device.
 */
#define POD_VENDOR_ID      0x03EB   // Arduino LLC
#define POD_PRODUCT_ID     0x2404   // Arduino Leonardo

int Tserial_init(Tserial_event_callback_t *serial_event_cb);
void Tserial_deinit(int);
void Tserial_sendData(char *buf, int len,int devh_index);
char *Tserial_getDataInBuffer(void);
int Tserial_getDataInSize(void);
static void* check_device_presence(void *arg);
static void* usb_read_thread(void* arg);
int usb_open(int);
void usb_close(int);
void usb_write(char *buf, int len,int devh_index);

Tserial_event_callback_t *Tserial_cb;

Tserial_interface_t Tserial_interface =
{
	Tserial_init,
	Tserial_sendData,
	Tserial_getDataInBuffer,
	Tserial_getDataInSize,
	Tserial_deinit
};

pthread_t usb_presence_thread, read_usb_thread[20];
int indicate_usbthread_exit = 0, indicate_readthread_exit[20] = { 0 };
int device_open[20] = {0}, device_presence[20] = {0};
static struct libusb_device_handle *devh[20] = {NULL};
/* The Endpoint address are hard coded. You should use lsusb -v to find
 * the values corresponding to your device.
 */
static int ep_in_addr  = 0x81;				// EP IN ADDR of POD
static int ep_out_addr = 0x02;				// EP OUT ADDR of POD

int pod_found=0;

Tserial_interface_t* get_Tserial_interface(void)
{
	return &Tserial_interface;
}

int Tserial_init(Tserial_event_callback_t *serial_event_cb)			//initialize the USB comm. and registering callback
{
	int rc;

#ifdef DEBUG_SERIAL
	_log_error(TAG, "Tserial init\n");
#endif
	if(serial_event_cb != NULL)
	{

#ifdef DEV_DEBUG_SERIAL
		_log_error(TAG, "serial_event_cb not null\n");
#endif
		Tserial_cb = serial_event_cb;


		/* Initialize libusb
		 */
		rc = libusb_init(NULL);
		if (rc < 0) {
			fprintf(stderr, "Error initializing libusb: %s\n", libusb_error_name(rc));
			return -1;
		}

		rc = pthread_create( &usb_presence_thread, NULL, check_device_presence, (void*) NULL);
		if(rc != 0)
		{
			_log_error(TAG, "Parse thread start error:%d\n", rc);
			return -1;
		}

#ifdef DEBUG_SERIAL
		_log_error(TAG, "out of check_dev_presence thread\n");
#endif
	}
	return 0;
}

void Tserial_deinit(int devh_index)			//temporarily commented
{

	indicate_readthread_exit[devh_index] = 1;
	if(read_usb_thread[devh_index] != 0)
	{
		pthread_join(read_usb_thread[devh_index], NULL);
		read_usb_thread[devh_index] = 0;
		indicate_readthread_exit[devh_index] = 0;
	}

	indicate_usbthread_exit = 1;
	if(usb_presence_thread != 0)
	{
		pthread_join(usb_presence_thread, NULL);
		indicate_usbthread_exit = 0;
		usb_presence_thread = 0;
	}

	libusb_exit(NULL);

}

void Tserial_sendData(char *buf, int len,int devh_index)			//send data to USB device
{

	usb_write(buf, len, devh_index);
}

char *Tserial_getDataInBuffer(void)
{

	return NULL;
}

int Tserial_getDataInSize(void)
{
return 0;
}


extern void toneplay_debug(int devh_index);
extern void startMic_debug(int devh_index);

struct libusb_device_descriptor desc;			//structure for USB device descriptor
libusb_device *dev;
int number_pods_connected=0;
char port_number;
USB_NODE* pod_list_head = NULL;

int node_index=0;

unsigned char* buffers[20];

static void* check_device_presence(void *arg)
{




	 int i = 0;
	libusb_device **devs;


	int r[20];
	ssize_t cnt;

	int usb_found[20]={0};

#ifdef DEBUG_USB
	_log_error(TAG, "Thread check device start\n");
#endif

	/*
	buffers[1]=(unsigned char*)calloc(1000,sizeof(unsigned char));
	buffers[6]=(unsigned char*)calloc(1000,sizeof(unsigned char));
	bufferInit(&usb_buffer[1],(unsigned char*)buffers[1],1000);
	bufferInit(&usb_buffer[6],(unsigned char*)buffers[6],1000);
*/
	for(int i=1; i<15; i++)
	{
		buffers[i]=(unsigned char*)calloc(1000,sizeof(unsigned char));
		bufferInit(&usb_buffer[i],(unsigned char*)buffers[i],1000);
	}

//	bufferInit(&usb_buffer[1],(unsigned char*)buffers[1],1000);
//	bufferInit(&usb_buffer[6],(unsigned char*)buffers[6],1000);
	printf("buffer[1] address:%p",&usb_buffer[1]);
	printf("buffer[6] address:%p",&usb_buffer[6]);
	while(indicate_usbthread_exit == 0)
	{

		Sleep(1000);
		cnt = libusb_get_device_list(NULL, &devs);			//get the list of USB devices on the system


		if (cnt < 0){
#ifdef DEBUG_USB
			_log_error(TAG, "USB list empty\n");
#endif
			continue;
		}



		while ((dev = devs[i++]) != NULL) {		//checking for every USB port




			r[i] = libusb_get_device_descriptor(dev, &desc);		//status for getting dev descriptor call
			if (r[i] < 0) {
#ifdef DEBUG_USB
				fprintf(stderr, "failed to get device descriptor");
#endif
			}



			if((desc.idVendor == POD_VENDOR_ID) &&	(desc.idProduct == POD_PRODUCT_ID))

			{

//					buffers[i]=(unsigned char*)calloc(1000,sizeof(unsigned char));		//Caution -> Do not forget to free the memory to avoid memory leaks
//				  // Initialise buffer handlings
//				bufferInit(&usb_buffer[i],(unsigned char*)buffers[i],1000);
//				printf("buffer[%d] address:%p",i,&usb_buffer[i]);

				port_number=libusb_get_port_number(dev);
				if(device_presence[i]==0)
				{
				printf("PID and VID matched, Portnumber%u,i:%d\n",port_number,i);
				}

#ifdef USER_DEBUG_L2
				printf("Setting usb_found[%d]:%d\n",i,usb_found[i]);
#endif
				usb_found[i] = 1;				// setting the usb_found flag to 1, indicating that the usb dev has been found
				if(device_presence[i] == 0)			//device's presence has discovered for the first time
				{
					device_presence[i] = 1;		//setting device's presence to 1 -- indicating that the device is present

#ifdef USER_DEBUG_L2
					_log_error(TAG, "PID and VID matched\n");
#endif

					Tserial_cb->on_event_cb(SERIAL_ATTACHED,i);		//callback when usb serial is attached
#ifdef USER_DEBUG_L2
					_log_error(TAG, "continuing\n");
#endif
					if(device_open[i] == 0)			//if device is getting opened for the first time
					{
#ifdef USER_DEBUG_L2
					_log_error(TAG, "POD dev. opening for the first time\n");
#endif

						r[i] = usb_open(i);			//opening the USB CDC device

						if(r[i] == 0)				//if it is opened successfully
						{



#ifdef USER_DEBUG_L2
					printf("Adding POD with devh_handle:%d\n",i);
#endif
					pod_list_head = add_usb_node(pod_list_head,i,port_number,++node_index);		//adding newly connected POD USB dev. to the LinkedList

					number_pods_connected=print_usb_nodes(pod_list_head);						//Print updated Linked-List of USB Nodes

#ifdef USER_DEBUG_L1
					_log_error(TAG, "\nPOD-%d Opened Successfully!!\n",node_index);
#endif

					device_open[i] = 1;  //setting the device_open flag to 1, indicating that the USB CDC dev is opened successfully.

#ifdef USER_DEBUG_L2
					_log_error(TAG, "\nBefore Serial Connected Callback\n");
#endif

							Tserial_cb->on_event_cb(SERIAL_CONNECTED,i);		//callback when usb serial is connected

#ifdef USER_DEBUG_L2
					_log_error(TAG, "\nAfter Serial Connected Callback\n");
#endif
							indicate_readthread_exit[i] = 0;
#ifdef DEV_DEBUG_L2
							printf("Creating pthread with i:%d\n",i);
#endif
							int *arg = malloc(sizeof(*arg));
							if ( arg == NULL ) {
								fprintf(stderr, "Couldn't allocate memory for thread arg.\n");
								exit(EXIT_FAILURE);
							}

							*arg = i;
							r[i] = pthread_create(&read_usb_thread[i], NULL, usb_read_thread, arg);



							if(r[i] != 0)
							{
#ifdef DEBUG_USB
								_log_error(TAG, "Parse thread start error:%d\n", r[i]);
#endif
								return NULL;
							}


						}
						else
						{
#ifdef DEBUG_USB
					_log_error(TAG, "USB Serial incorrect driver\n");
#endif

							Tserial_cb->on_event_cb(SERIAL_INCORRECT_DRIVER,i);		//callback when usb device is incorrect
						}
					}		//device open if

					 if(device_open[i]==1)
					{

#ifdef USER_DEBUG_L2
					_log_error(TAG, "device was opened\n");
#endif
					}

					 //device discovered for the first time
				}		//device presence if
				else if(device_open[i] == 0)		//if the device was not opened
				{

#ifdef USER_DEBUG_L2
					_log_error(TAG, "device not opened 0,trying to open again\n");
#endif

					r[i] = usb_open(i);				//open the USB CDC device
					if(r[i] == 0)					//if opened successfully
					{
						device_open[i] = 1;  //setting the device_open flag to 1, indicating that the USB CDC dev is opened successfully.
						Tserial_cb->on_event_cb(SERIAL_CONNECTED,i);			//callback when USB serial is connected
						int *arg2 = malloc(sizeof(*arg2));
						if ( arg2 == NULL ) {
							fprintf(stderr, "Couldn't allocate memory for thread arg.\n");
							exit(EXIT_FAILURE);
						}

						*arg2 = i;
						r[i] = pthread_create(&read_usb_thread[i], NULL, usb_read_thread, arg2);

						if(r[i] != 0)
						{
							_log_error(TAG, "Parse thread start error:%d\n", r[i]);
							return NULL;
						}
					}
				}



			}	//The PID and VID matches

			else{		//if the ith USB device doesn't have the POD descriptors then reset its usb_found flag and delete that node from the linked list
				usb_found[i]=0;
			//	remove_usb_node(pod_list_head,i);

			}

		}	//while ends here
#ifdef USER_DEBUG_L2

printf("out of for loop\n");
#endif
		int total_pods = get_number_pods_connected();
#ifdef DEV_DEBUG_L2
		printf("total number of pods:%d\n",total_pods);
		number_pods_connected=print_usb_nodes(pod_list_head);						//Print updated Linked-List of USB Nodes
#endif
		if(total_pods>0){
		for(int p=1; p<=total_pods; p++){
#ifdef USER_DEBUG_L2
			printf("in for loop,p:%d\n",p);
#endif
			/*
		for(int l=0; l<k; l++){

			if(blacklist[l]==p){breakflag=1;break;
			printf("breaking from inner loop\n");
			}
		}
		if(breakflag==1){breakflag=0;
		printf("continuing from outer loop\n");
		continue;
		}
*/
		int pod_handle=get_usb_handle_by_index(p);
		 if(pod_handle<0){
			 //invalid pod handle
			 continue;
		 }
#ifdef DEV_DEBUG_L2
						printf("pods connected:%d,handle:%d\n",get_number_pods_connected(),pod_handle);
						printf("usb_found[%d]=%d\n",pod_handle,usb_found[pod_handle]);
#endif

						if(usb_found[pod_handle] == 0)		//if the USB CDC dev has not been found -- when the VID and PID doesn't match with ToneTag POD
						{
#ifdef DEV_DEBUG_L2
							printf("pod_handle before removing node:%d ",pod_handle);
#endif



#ifdef DEV_DEBUG_L2
							printf("number of pods after removal:%d\n",get_number_pods_connected());
#endif

#ifdef DEV_DEBUG_L2
									_log_error(TAG, "POD not found, i:%d\n",pod_handle);
#endif
	/*						device_presence[pod_handle] = 0;		//disable the device presence flag, indicating that the device is not present
							if(device_open[pod_handle] == 1)	//if the device was opened
							{
								indicate_readthread_exit[pod_handle] = 1;
								pthread_join(read_usb_thread[pod_handle], NULL);
								usb_close(pod_handle);				//close the USB device
								device_open[pod_handle] = 0;
								Tserial_cb->on_event_cb(SERIAL_DETACHED,pod_handle);		//callback when usb serial is detached
								Tserial_cb->on_event_cb(SERIAL_DISCONNECTED,pod_handle);	//callback when usb serial is disconnected
							}
	*/




	/*						int nodei=get_node_index_by_handle_index(pod_handle);
							blacklist[k++]=nodei;
							remove_usb_node(&pod_list_head,pod_handle);
*/
							/*							remove_usb_node(&pod_list_head,pod_handle);
							number_pods_connected-=1;
							node_index-=1;
							total_pods-=1;
*/
/*							int usb_handle=get_usb_handle_by_index(node_index);
							usb_found[usb_handle]=1;
							device_open[pod_handle] = 1;
*/
#ifdef USER_DEBUG_L2

							printf("index after removal:%d",node_index);

							printf("Nodes after Removal\n");
							number_pods_connected=print_usb_nodes(pod_list_head);
#endif

#ifdef USER_DEBUG_L2

							printf("continuing");
#endif
							//	break;

						}


				}
		}

#ifdef USER_DEBUG_L2

printf("freeing the device list\n");
#endif
		libusb_free_device_list(devs, 1);				//Frees a list of devices previously discovered using libusb_get_device_list()
		i = 0;
	}

	_log_error(TAG, "check device presence stop\n");
	return NULL;
}


int get_number_pods_connected()
{
	return number_pods_connected;
}


extern int sem;
void *usb_read_thread(void* arg)
{
	int devh_index =*((int* )arg);

#ifdef DEBUG_USB
	_log_error(TAG, "USB read thread,devh_index:%d\n",devh_index);
#endif

	int size = 100, actual_length = 0;
	unsigned char data[100];

#ifdef DEV_DEBUG_L2
	_log_error(TAG, "USB read thread,devh_index:%d\n",devh_index);
#endif

#ifdef DEBUG_USB
	_log_error(TAG, "usb read thread begin\n");
#endif
	free(arg);
	while(indicate_readthread_exit[devh_index] == 0 )
	{

		/* To receive characters from the device initiate a bulk_transfer to the
		 * Endpoint with address ep_in_addr.
		 */

		int rc = libusb_bulk_transfer(devh[devh_index], ep_in_addr, data, size, &actual_length,
									  0);
		if (rc == LIBUSB_ERROR_TIMEOUT) {
			_log_error(TAG, "timeout (%d)\n", actual_length);
			goto goout;
		} else if (rc < 0) {
			_log_error(TAG, "Error while waiting for char\n");
			goto goout;
		}
		else
		{

#ifdef DEBUG_USB
			printf("\ndevh_index:%d ",devh_index);
			for(int i = 0; i < actual_length; i++)
				_log_error(TAG, "%x ", data[i]);			//printing the USB received data from the ToneTag POD
#endif

				Tserial_cb->on_data_rx_cb((char *)data, actual_length, devh_index);


		}
	}
goout:
#ifdef DEBUG_USB
	_log_error(TAG, "usb read thread end\n");
#endif
	return NULL;
}



#define ACM_CTRL_DTR   0x01
#define ACM_CTRL_RTS   0x02

/* We use a global variable to keep the device handle
 */

void usb_write(char *buf, int len,int devh_index)			//write to the USB via libusb call
{
    /* To send a char to the device simply initiate a bulk_transfer to the
     * Endpoint with address ep_out_addr.
     */
	//printf("devh_index in usb_write:%d\n",devh_index);
    int actual_length;
    if (libusb_bulk_transfer(devh[devh_index], ep_out_addr, (unsigned char *)buf, len,
                             &actual_length, 0) < 0) {
        _log_error(TAG, "Error while sending char\n");
    }
}

int usb_read(unsigned char *data, int size, int devh_index)		//read the USB via libusb call
{
    /* To receive characters from the device initiate a bulk_transfer to the
     * Endpoint with address ep_in_addr.
     */
    int actual_length;
    int rc = libusb_bulk_transfer(devh[devh_index], ep_in_addr, data, size, &actual_length,
                                  0);
    if (rc == LIBUSB_ERROR_TIMEOUT) {
    	_log_error(TAG, "timeout (%d)\n", actual_length);
        return -1;
    } else if (rc < 0) {
    	_log_error(TAG, "Error while waiting for char\n");
        return -1;
    }

    return actual_length;
}


int usb_open(int devh_index)				//open USB dev for comm. matching with the PID and VID
{


#ifdef DEV_DEBUG_L2
	_log_error(TAG, "\nOpening USB device\n");
#endif
	int rc;
	int if_num;


	/* Set debugging output to max level.
	 */
	libusb_set_option(NULL, 3);

	/* Look for a specific device and open it.
	 */
	//devh = libusb_open_device_with_vid_pid(NULL, POD_VENDOR_ID, POD_PRODUCT_ID);
	int open_stat=libusb_open(dev,&devh[devh_index]);
	if(open_stat==0){
#ifdef DEV_DEBUG_L2
		_log_error(TAG, "libusb opened,stat:%d\n",open_stat);
#endif
	}

#ifdef DEV_DEBUG_L2
	_log_error(TAG, "after opening\n");
#endif
	//_log_error(TAG, devh);
	if (!devh[devh_index]) {
		_log_error(TAG, "Error finding USB device\n");
		rc = -1;
		goto out;
	}
/*
				unsigned char uSerialNumber[255] = {};
				int iSerialNumberSize = libusb_get_string_descriptor_ascii(devh, desc.iSerialNumber, uSerialNumber, sizeof(uSerialNumber));
				printf("SerialNumber-%s\n",uSerialNumber);
*/
	/* As we are dealing with a CDC-ACM device, it's highly probable that
	 * Linux already attached the cdc-acm driver to this device.
	 * We need to detach the drivers from all the USB interfaces. The CDC-ACM
	 * Class defines two interfaces: the Control interface and the
	 * Data interface.
	 */
	for (if_num = 0; if_num < 2; if_num++) {
		if (libusb_kernel_driver_active(devh[devh_index], if_num)) {
			libusb_detach_kernel_driver(devh[devh_index], if_num);
		}
		rc = libusb_claim_interface(devh[devh_index], if_num);
#ifdef DEV_DEBUG_L2
		_log_error(TAG, "Claim interface,rc:%d\n",rc);
#endif

		if (rc < 0) {
			_log_error(TAG, "Error claiming interface: %s\n",
					libusb_error_name(rc));
			goto out;
		}

	}

	/* Start configuring the device:
	 * - set line state
	 */
	rc = libusb_control_transfer(devh[devh_index], 0x21, 0x22, ACM_CTRL_DTR | ACM_CTRL_RTS,
								0, NULL, 0, 0);
#ifdef DEV_DEBUG_L2
	_log_error(TAG, "Libusb Control transfer set line state,rc:%d\n",rc);
#endif
	if (rc < 0) {
		_log_error(TAG, "Error during control transfer: %s\n",
				libusb_error_name(rc));
		goto release;
	}

	/* - set line encoding: here 9600 8N1
	 * 9600 = 0x2580 ~> 0x80, 0x25 in little endian
	 */
	unsigned char encoding[] = { 0x80, 0x25, 0x00, 0x00, 0x00, 0x00, 0x08 };
	rc = libusb_control_transfer(devh[devh_index], 0x21, 0x20, 0, 0, encoding,
								sizeof(encoding), 0);
#ifdef DEV_DEBUG_L2
	_log_error(TAG, "Libusb Control transfer set line encoding,rc:%d\n",rc);
#endif
	if (rc < 0) {
		_log_error(TAG, "Error during control transfer: %s\n",
				libusb_error_name(rc));
		goto release;
	}
	return 0;

release:
	libusb_release_interface(devh[devh_index], 0);
out:
	if (devh[devh_index])
			libusb_close(devh[devh_index]);

	return rc;
}

void usb_close(int devh_index)			//close the USB dev.
{
	if(devh[devh_index] != NULL)
	{	printf("Closing USB with devh_index %d:",devh_index);
		libusb_release_interface(devh[devh_index], 0);
		libusb_close(devh[devh_index]);
	}
	else{
		printf("Device handle NULL, devh_index:%d",devh_index);
	}
}

void write_char(unsigned char c,int devh_index)
{
    /* To send a char to the device simply initiate a bulk_transfer to the
     * Endpoint with address ep_out_addr.
     */
    int actual_length;
    if (libusb_bulk_transfer(devh[devh_index], ep_out_addr, &c, 1,
                             &actual_length, 0) < 0) {
        fprintf(stderr, "Error while sending char\n");
    }
}

int read_chars(unsigned char * data, int size, int devh_index)
{
    /* To receive characters from the device initiate a bulk_transfer to the
     * Endpoint with address ep_in_addr.
     */
    int actual_length;
    int rc = libusb_bulk_transfer(devh[devh_index], ep_in_addr, data, size, &actual_length,
                                  1000);
    if (rc == LIBUSB_ERROR_TIMEOUT) {
        printf("timeout (%d)\n", actual_length);
        return -1;
    } else if (rc < 0) {
        fprintf(stderr, "Error while waiting for char\n");
        return -1;
    }

    return actual_length;
}



